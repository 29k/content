#!/usr/bin/env node

import fs from 'fs';
import path from 'path';
import createRequire from './lib/require.cjs';
import {
  preprocessSingles,
  preprocessConversations,
  preprocessCourses,
} from './lib/index.js';
import validateContent from './lib/validation/index.js';

const cjsRequire = createRequire(import.meta.url);

const getFiles = (contentType, { asObject = false } = {}) => {
  const normalizedPath = path.resolve('./src', contentType);

  if (!fs.existsSync(normalizedPath)) return [];

  const files = fs.readdirSync(normalizedPath);

  if (asObject) {
    return files.reduce(
      (all, file) => ({
        ...all,
        [path.basename(file, '.json')]: cjsRequire(
          path.join(normalizedPath, file),
        ),
      }),
      {},
    );
  } else {
    return files.map((f) => cjsRequire(path.join(normalizedPath, f)));
  }
};

/*
 Generates i18n-friendly structure

 {
  locale: 'sv',
  namespace: UI.Common.Button,
  resources: {
    saveButton: {
      text: 'Save'
    },
    saveButton_saving: {
      text: 'Saving'
    },
  }
 }
*/

const generateI18NResources = (translatedContent) =>
  translatedContent.reduce((all, currenContent) => {
    const namespaces = Object.keys(currenContent);

    namespaces.forEach((namespace) => {
      const locales = Object.keys(currenContent[namespace]);

      locales.forEach((locale) => {
        all[locale] = {
          ...all[locale],
          [namespace]: currenContent[namespace][locale],
        };
      });
    });

    return all;
  }, {});

const contentLib = {
  meditations: getFiles('meditations'),
  exercises: getFiles('exercises2'),
  lessons: getFiles('lessons'),
};

const singles = preprocessSingles(contentLib)(getFiles('singles'));
const courses = preprocessCourses(contentLib)(getFiles('courses'));

const content = validateContent({
  COURSES: courses.courses,

  PRINCIPLES: getFiles('principles', { asObject: true }),

  PSYCHOLOGICAL_TESTS: getFiles('psychologicalTests'),

  EXERCISES: [...getFiles('exercises'), ...singles.exercises],
  CONVERSATIONS: [
    ...preprocessConversations(getFiles('conversations')),
    ...singles.conversations,
    ...courses.conversations,
  ],
  FEATURED_CONTENT: getFiles('featuredContent', {
    asObject: true,
  }),
  CATEGORIES: getFiles('categories'),

  // i18n-enabled content
  i18nResources: generateI18NResources([
    getFiles('backend', { asObject: true }),
    getFiles('bot', { asObject: true }),
    getFiles('emails', { asObject: true }),
    getFiles('faq', { asObject: true }),
    getFiles('legal', { asObject: true }),
    getFiles('ui', { asObject: true }),
  ]),
});

const data = JSON.stringify(content);
if (process.argv.length > 2) {
  fs.writeFileSync(process.argv[2], data);
} else {
  process.stdout.write(data);
}
