import { path, propOr, propEq } from 'ramda';
import { ContentValidationError } from '../errors.js';

const getConversationById = (id, data) =>
  path(['indexedContent', 'CONVERSATIONS', id], data);

const noInvalidConversationRef = (lesson, data) => {
  const [course] = propOr([], 'path', data);
  if (lesson.conversations) {
    lesson.conversations.forEach((c) => {
      if (!c.conversationId)
        throw new ContentValidationError(
          `[${course.id} / ${lesson.id}] field 'conversationId' is undefined`,
        );

      if (!getConversationById(c.conversationId, data)) {
        throw new ContentValidationError(
          `[${course.id} / ${lesson.id}] Conversation ${c.conversationId} does not exist`,
        );
      }
    });
  }
};

// Currently, we do not support navigating to a conversation
// that has NOT been declared in the lesson's `conversations` field
const noExternalConversationRefs = (button, data) => {
  if (!button.conversationId) return;
  const [, section, conversation, lesson, course] = data.path;
  const { conversationId } = button;
  const targetConversation = lesson.conversations.find(
    propEq('conversationId', conversationId),
  );

  if (!targetConversation) {
    throw new ContentValidationError(
      `[Course: ${course.id} / Lesson: ${lesson.id}]: A button in section '${section.id}' of conversation '${conversation.id}' references a conversation ('${conversationId}') which has not been declared by lesson '${lesson.id}' (${lesson.name}). We do not currently support this.\n\n\tHint: If this is not a mistake, add the conversation to the lesson's 'conversations' array`,
    );
  }
};

export { noInvalidConversationRef, noExternalConversationRefs };
