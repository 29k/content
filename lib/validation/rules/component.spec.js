import { ContentValidationError } from '../errors.js';
import {
  noInvalidReferences,
  noEmptyButtonGroups,
  noButtonsWithoutAGroup,
  containsOnlyValidButtons,
  containsOnlyValidTextComponents,
  containsOnlyValidVideoRefs,
  buttonsMustHaveTargetRef,
  validNextConversationTarget,
  validButtonTypes,
  hasProps,
} from './component.js';

describe('noEmptyButtonGroups', () => {
  it('should reject empty button groups', () => {
    const ButtonGroups = [
      {
        component: 'ButtonGroup',
        content: [],
      },
      {
        component: 'ButtonGroup',
      },
    ];

    ButtonGroups.forEach((ButtonGroup) => {
      expect(() => {
        noEmptyButtonGroups(ButtonGroup);
      }).toThrow(ContentValidationError);
    });
  });
});

describe('noButtonsWithoutAGroup', () => {
  it('should reject buttons without enclosing group', () => {
    const section = {
      id: 'some-section',
      content: validButtonTypes.map((type) => ({
        component: type,
      })),
    };

    section.content.forEach((component) => {
      expect(() => {
        noButtonsWithoutAGroup(component, {
          path: [section, { id: 'someId' }],
        });
      }).toThrow(ContentValidationError);
    });
  });

  it('should ignore components other than Buttons', () => {
    const section = {
      id: 'some-section',
      content: [{ component: 'TextGroup' }, { component: 'Exercise' }],
    };

    section.content.forEach((component) => {
      expect(() => {
        noButtonsWithoutAGroup(component, {
          path: [section, { id: 'someId' }],
        });
      }).not.toThrow();
    });
  });
});

describe('containsOnlyValidVideoRefs', () => {
  it('should reject VideoStories with 0 videos', () => {
    const VideoStories = {
      component: 'VideoStories',
      content: [],
    };

    expect(() => {
      containsOnlyValidVideoRefs(VideoStories, {
        path: [VideoStories, { id: 'sectionId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('should reject Videos lacking a videoUri', () => {
    const VideoStories = {
      component: 'VideoStories',
      content: [{ imageUri: 'lol' }],
    };

    expect(() => {
      containsOnlyValidVideoRefs(VideoStories, {
        path: [VideoStories, { id: 'sectionId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('should reject Videos lacking an imageUri', () => {
    const VideoStories = {
      component: 'VideoStories',
      content: [{ videoUri: 'lol' }],
    };

    expect(() => {
      containsOnlyValidVideoRefs(VideoStories, {
        path: [VideoStories, { id: 'sectionId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('should accept VideoStories with valid videos', () => {
    const VideoStories = {
      component: 'VideoStories',
      content: [{ videoUri: 'haha', imageUri: 'lol' }],
    };

    expect(() => {
      containsOnlyValidVideoRefs(VideoStories, {
        path: [VideoStories, { id: 'sectionId' }],
      });
    }).not.toThrow(ContentValidationError);
  });
});

describe('containsOnlyValidButtons', () => {
  it('should reject ButtonGroups with invalid button types', () => {
    const ButtonGroup = {
      component: 'ButtonGroup',
      content: [{ component: 'SomeWeirdButton' }],
    };

    expect(() => {
      containsOnlyValidButtons(ButtonGroup, {
        path: [ButtonGroup, { id: 'sectionId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('should accept ButtonGroups containing only valid button types', () => {
    const ButtonGroup = {
      component: 'ButtonGroup',
      content: validButtonTypes.map((type) => ({
        component: type,
      })),
    };

    expect(() => {
      containsOnlyValidButtons(ButtonGroup, { id: 'sectionId' });
    }).not.toThrow();
  });
});

describe('noInvalidReferences', () => {
  it('should accept a valid reference to a conversation section', () => {
    const buttons = [
      {
        component: 'Button',
        sectionId: 'exists',
      },
      {
        component: 'Button',
        sectionId: 'END',
      },
      {
        component: 'NextConversationButton',
        conversationId: 'testConv',
      },
    ];

    const testSection = {
      id: 'test',
    };

    const testConv = {
      id: 'testConv',
      alias: 'testConvAlias',
      sections: [testSection, { id: 'exists' }],
    };

    const data = {
      indexedContent: {
        CONVERSATIONS: {
          testConv: {
            id: 'testConv',
            alias: 'testConvAlias',
            sections: {
              test: testSection,
              exists: { id: 'exists' },
            },
          },
        },
      },
      path: [{ component: 'ButtonGrou' }, testSection, testConv],
    };

    buttons.forEach((button) => {
      expect(() => {
        noInvalidReferences(button, data);
      }).not.toThrow();
    });
  });

  it('should reject invalid section references', () => {
    const testSection = { id: 'test' };

    const testConv = {
      id: 'testConv',
      alias: 'testConvAlias',
      sections: [testSection, { id: 'exists' }],
    };

    const index = {
      indexedContent: {
        CONVERSATIONS: {
          testConv: {
            alias: 'testConvAlias',
            sections: {
              test: testSection,
              exists: { id: 'exists' },
            },
          },
        },
      },
      path: [{ component: 'ButtonGroup' }, testConv, testSection],
    };

    const buttons = [
      {
        component: 'Button',
        sectionId: 'dudette',
      },
      {
        component: 'Button',
        conversationId: 'whatever',
        sectionId: 'whatever',
      },
    ];

    buttons.forEach((button) => {
      expect(() => {
        noInvalidReferences(button, index);
      }).toThrow(ContentValidationError);
    });
  });
});

describe('buttonsMustHaveTargetRef', () => {
  it('should accept Button components with either, or both of, conversationId and sectionId', () => {
    expect(() => {
      buttonsMustHaveTargetRef(
        { component: 'Button', conversationId: 'hehu' },
        {},
      );
      buttonsMustHaveTargetRef({ component: 'Button', sectionId: 'hehu' }, {});
    }).not.toThrow();
  });

  it('should reject Button components that specify neither conversationId nor sectionID', () => {
    const data = {
      path: [
        null,
        { id: 'current-section-id' },
        { id: 'current-conversation-id' },
      ],
    };

    expect(() => {
      buttonsMustHaveTargetRef({ component: 'Button' }, data);
    }).toThrow(ContentValidationError);

    expect(() => {
      buttonsMustHaveTargetRef({ component: 'NextConversationButton' }, data);
    }).toThrow(ContentValidationError);
  });
});

describe('validNextConversationTarget', () => {
  const data = {
    path: [
      null,
      { id: 'current-section-id' },
      { id: 'current-conversation-id' },
    ],
  };

  it('should accept target if not the same conversation', () => {
    expect(() => {
      validNextConversationTarget(
        {
          component: 'NextConversationButton',
          conversationId: 'next-conversation-id',
        },
        data,
      );
    }).not.toThrow();
  });

  it('should reject if conversationId is empty', () => {
    expect(() => {
      validNextConversationTarget(
        {
          component: 'NextConversationButton',
          conversationId: '',
        },
        data,
      );
    }).toThrow(ContentValidationError);
  });

  it('should reject if same conversation', () => {
    expect(() => {
      validNextConversationTarget(
        {
          component: 'NextConversationButton',
          conversationId: 'current-conversation-id',
        },
        data,
      );
    }).toThrow(ContentValidationError);
  });
});

describe('containsOnlyValidTextComponents', () => {
  it('should reject TextGroups with invalid Text types', () => {
    const TextGroup = {
      component: 'TextGroup',
      content: [{ component: 'SomeWeirdComponent' }],
    };

    expect(() => {
      containsOnlyValidTextComponents(TextGroup, {
        path: [TextGroup, { id: 'sectionId' }, { id: 'conversationId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('should not reject TextGroups with only valid Text types', () => {
    const TextGroup = {
      component: 'TextGroup',
      content: [
        { component: 'Text' },
        { component: 'Subtitle' },
        { component: 'SmallPrint' },
        { component: 'Quote' },
        { component: 'MiniBadge' },
        { component: 'HeaderImage' },
        { component: 'Guiding' },
        { component: 'CollapsibleList' },
      ],
    };

    expect(() => {
      containsOnlyValidTextComponents(TextGroup, {
        path: [TextGroup, { id: 'sectionId' }, { id: 'conversationId' }],
      });
    }).not.toThrow(ContentValidationError);
  });
});

describe('hasProps', () => {
  it('rejects components missing the specified props', () => {
    const invalidComponent = {
      component: 'LOL',
    };
    expect(() => {
      hasProps('LOL', ['nasty'])(invalidComponent, {
        path: [invalidComponent, { id: 'sectionId' }, { id: 'conversationId' }],
      });
    }).toThrow(ContentValidationError);
  });

  it('ignores non-target components', () => {
    const invalidComponent = {
      component: 'haha',
    };
    expect(() => {
      hasProps('LOL', ['nasty'])(invalidComponent, {
        path: [invalidComponent, { id: 'sectionId' }, { id: 'conversationId' }],
      });
    }).not.toThrow(ContentValidationError);
  });
});
