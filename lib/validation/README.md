# Content Validation

This folder contains a simple framework for testing our
content in general, and conversations in particular. For
example, most errors related to the conversational UI are
due to components referencing conversation- or section IDs
that do not exist.

To test a piece of content, there are two important things
to know about: rules and validators.

## Validators
Validators are responsible for selecting a subset of data,
in the form of a list, and passing each element of that list
to a set of rules (see below). For example, a "Conversation
validator" would get a list of conversations, passing each
conversation to each rule to be validated.

To create a validator, use the `createValidator` function,
which has the following signature:

```js
createValidator = (select) => (rules) => (content, data)
```

`select` - the selector function that determines the data to
pass to each rule. Must return an array, even if empty.

`rules` - an array of rules (see below)

`content` - the content from which the select() function
selects a subset

`data` - an object containing data that may be relevant for
a rule:

- `path` - A bottom-up list of the path to the currently
  considered node. For example, if currently considering a
  component (e.g., Button), the path would be
  `[parentSection, parentConversation, rootContent]`
- indexedContent - an index of all content, indexed by ID
  (See example below)

### Example: Adding a validator
```{js}
const conversationValidator = createValidator(prop('CONVERSATIONS'));
```
  
## Rules
Rules are small functions that assert that a piece of
content satisfies some property (e.g., a lesson must
have an `author` field).

A rule should have the following signature:

```{js}
const someRule = (content, data) => { ... }
```

`content` - the piece of content currently being considered.
The concrete type of content will depend on the parent
validator.

`data` - an object containing data that may be relevant for
a rule:

- `path` - A bottom-up list of the path to the currently
  considered node. For example, if currently considering a
  component (e.g., Button), the path would be
  `[parentSection, parentConversation, rootContent]`
- indexedContent - an index of all content, indexed by ID
  (See example below)

On balance, you'll be adding rules more often than you will
validators.
    
## Example: Adding a rule
```{js}
// rules/component.js

// 1: define the rule
const noInvalidRefs = (component, data) => {
  // Filter for the component(s) of interest
  if(component.component !== 'SomeComponent') return;
  
  // Look up the target section
  const {conversationId, sectionId} = component
  const target = path(['indexedContent', 'CONVERSATIONS', conversationId, sectionId], data)
  
  if(!target) {
    const [section, conversation] = data.path

    throw new ContentValidationError(`section ${section.id}
    in conversation ${conversation.id} references
    ${sectionId} in ${conversationId}, which does not exist!`)
  }
  
  
  // Yay success!
}

// index.js
const { noInvalidRefs } = require('./rules/component')

// 2: Add the rule to the appropriate validator
// Note: validators can be nested
const rootValidator = createRootValidator(reporter)([
  conversationValidator([
    sectionValidator([
      componentValidator([
        noInvalidRefs // <-- add rule here
      ])
    ])
  ])
])

```
