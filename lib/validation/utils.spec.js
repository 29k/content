import { groupContentByLang, indexContent } from './utils.js';

describe('groupContentByLang', () => {
  it('should group content by language', () => {
    const content = {
      CONVERSATIONS: [
        {
          en: {
            id: 'Hehu',
          },
          sv: {
            id: 'sv-Hehu',
          },
        },
      ],
      COURSES: [
        {
          en: {
            id: 'Hehu',
          },
          sv: {
            id: 'sv-Hehu',
          },
        },
      ],
      nonTranslatedContent: [{ name: 'Robin' }],
    };

    expect(groupContentByLang(content)).toEqual([
      [
        'en',
        {
          CONVERSATIONS: [{ id: 'Hehu' }],
          COURSES: [{ id: 'Hehu' }],
          nonTranslatedContent: [{ name: 'Robin' }],
        },
      ],
      [
        'sv',
        {
          CONVERSATIONS: [{ id: 'sv-Hehu' }],
          COURSES: [{ id: 'sv-Hehu' }],
          nonTranslatedContent: [{ name: 'Robin' }],
        },
      ],
    ]);
  });
});

describe('indexContent', () => {
  it('should index content with IDs', () => {
    const content = {
      CONVERSATIONS: [
        {
          id: 'Hehu',
          sections: [{ id: 'someSectionId' }],
        },
      ],
      COURSES: [
        {
          id: 'Hehu',
        },
      ],
      EXERCISES: [
        {
          id: 'exercise',
        },
      ],
      backend: {},
    };

    expect(indexContent(content)).toEqual({
      CONVERSATIONS: {
        Hehu: {
          id: 'Hehu',
          sections: { someSectionId: { id: 'someSectionId' } },
        },
      },
      COURSES: {
        Hehu: { id: 'Hehu' },
      },
      EXERCISES: {
        exercise: { id: 'exercise' },
      },
    });
  });
});
