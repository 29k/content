import { rootSectionExists, idIsDefined } from './rules/conversation.js';
import {
  noInvalidConversationRef,
  noExternalConversationRefs,
} from './rules/lesson.js';
import { noInvalidConversationRef as exerciseNoInvalidConversationRef } from './rules/exercise.js';
import { noStandaloneTexts } from './rules/section.js';
import {
  buttonsMustHaveTargetRef,
  containsOnlyValidButtons,
  noEmptyButtonGroups,
  noButtonsWithoutAGroup,
  noInvalidReferences,
  validNextConversationTarget,
  containsOnlyValidTextComponents,
  containsOnlyValidVideoRefs,
  hasProps,
} from './rules/component.js';
import {
  createRootValidator,
  conversationValidator,
  sectionValidator,
  courseValidator,
  lessonValidator,
  exerciseValidator,
  componentValidator,
  buttonGroupChildValidator,
  conversationConnectionValidator,
} from './validators.js';

const reporter = {
  errors: [],
  error: (message, tag) =>
    reporter.errors.push(tag ? `[${tag}] ${message}` : message),
};

// Add lesson-level rules here
// e.g., rule(lesson, data)
const lessonRules = [
  noInvalidConversationRef,
  conversationConnectionValidator([
    sectionValidator([
      componentValidator([
        containsOnlyValidTextComponents,
        buttonGroupChildValidator([
          noExternalConversationRefs,
          validNextConversationTarget,
          noInvalidReferences,
        ]),
      ]),
    ]),
  ]),
];

// Add course-level rules here
// e.g., rule(course, data)
const courseRules = [lessonValidator(lessonRules)];

const exerciseRules = [
  exerciseNoInvalidConversationRef,
  conversationConnectionValidator([
    sectionValidator([
      componentValidator([
        buttonGroupChildValidator([
          noExternalConversationRefs,
          noInvalidReferences,
        ]),
      ]),
    ]),
  ]),
];

// Add section-level rules here
// e.g., rule(component, data)
const componentRules = [
  noEmptyButtonGroups,
  noButtonsWithoutAGroup,
  containsOnlyValidButtons,
  containsOnlyValidVideoRefs,
  buttonGroupChildValidator([
    buttonsMustHaveTargetRef,
    hasProps('TestCompleteButton', ['categoryId']),
  ]),
];

// Add section-level rules here
// e.g., rule(section, data)
const sectionRules = [componentValidator(componentRules), noStandaloneTexts];

// Add conversation-level rules here
// e.g., rule(conversation, data)
const conversationRules = [
  sectionValidator(sectionRules),
  rootSectionExists,
  idIsDefined,
];

export default createRootValidator(reporter)([
  courseValidator(courseRules),
  exerciseValidator(exerciseRules),
  conversationValidator(conversationRules),
]);
