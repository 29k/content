import { preprocessSingles } from './single.js';

describe('preprocessSingles', () => {
  const meditations = [
    {
      lang: {
        id: 'meditation-id',
        name: 'Meditaiton Name',
        description: 'meditation description',
        audioUri: 'audioUri-value',
      },
    },
  ];
  const exercises = [
    {
      lang: {
        id: 'exercise-id',
        name: 'Exercise Name',
        description: 'exercise description',
      },
    },
  ];

  const singles = [
    {
      lang: {
        id: 'single-id',
        type: 'meditation',
        name: 'Test-single',
        sections: [
          {
            type: 'meditation',
            id: 'meditation-section-id',
            refId: 'meditation-id',
          },
          {
            type: 'exercise',
            id: 'exercise-section-id',
            refId: 'exercise-id',
          },
          {
            type: 'editorialWithImage',
            id: 'some-section-id',
            topImageUri: 'image-url',
            title: "You're done",
            content: 'some content',
          },
        ],
      },
    },
    {
      lang: {
        id: 'single-id-2',
        type: 'meditation',
        name: 'Test-single',
        sections: [
          {
            type: 'meditation',
            id: 'meditation-section-id',
            refId: 'meditation-id',
          },
        ],
      },
    },
    {
      lang: {
        id: 'single-id-3',
        type: 'exercise',
        name: 'Test-single',
        sections: [
          {
            type: 'exercise',
            id: 'exercise-section-id',
            refId: 'exercise-id',
          },
        ],
      },
    },
  ];

  describe('conversations', () => {
    it('adds buttons to progress to the next section', () => {
      expect(
        preprocessSingles({ exercises, meditations })(singles).conversations,
      ).toEqual([
        {
          lang: {
            id: 'conversation-single-id',
            name: 'Test-single',
            root: 'meditation-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: '# Meditaiton Name' },
                      { component: 'Text', content: 'meditation description' },
                    ],
                  },
                  {
                    component: 'AudioPlayer',
                    content: { duration: undefined, uri: 'audioUri-value' },
                  },
                ],
                id: 'meditation-section-id',
              },
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      {
                        component: 'MiniBadge',
                        content: {
                          url: 'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
                        },
                      },
                      { component: 'Text', content: '# Exercise Name' },
                      { component: 'Text', content: 'exercise description' },
                    ],
                  },
                ],
                id: 'exercise-section-id',
              },
              {
                content: [
                  {
                    component: 'Image',
                    content: { url: 'image-url' },
                    props: undefined,
                  },
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: "# You're done" },
                      { component: 'Text', content: 'some content' },
                    ],
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'some-section-id',
              },
            ],
            type: 'meditation',
          },
        },
        {
          lang: {
            id: 'conversation-single-id-2',
            name: 'Test-single',
            root: 'meditation-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: '# Meditaiton Name' },
                      { component: 'Text', content: 'meditation description' },
                    ],
                  },
                  {
                    component: 'AudioPlayer',
                    content: { duration: undefined, uri: 'audioUri-value' },
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'meditation-section-id',
              },
            ],
            type: 'meditation',
          },
        },
        {
          lang: {
            id: 'conversation-single-id-3',
            name: 'Test-single',
            root: 'exercise-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      {
                        component: 'MiniBadge',
                        content: {
                          url: 'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
                        },
                      },
                      { component: 'Text', content: '# Exercise Name' },
                      { component: 'Text', content: 'exercise description' },
                    ],
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'exercise-section-id',
              },
            ],
            type: 'exercise',
          },
        },
      ]);
    });

    it('adds a button to complete the exercise', () => {
      const processedConversations = preprocessSingles({
        exercises,
        meditations,
      })(singles).conversations;

      expect(processedConversations).toEqual([
        {
          lang: {
            id: 'conversation-single-id',
            name: 'Test-single',
            root: 'meditation-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: '# Meditaiton Name' },
                      { component: 'Text', content: 'meditation description' },
                    ],
                  },
                  {
                    component: 'AudioPlayer',
                    content: { duration: undefined, uri: 'audioUri-value' },
                  },
                ],
                id: 'meditation-section-id',
              },
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      {
                        component: 'MiniBadge',
                        content: {
                          url: 'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
                        },
                      },
                      { component: 'Text', content: '# Exercise Name' },
                      { component: 'Text', content: 'exercise description' },
                    ],
                  },
                ],
                id: 'exercise-section-id',
              },
              {
                content: [
                  {
                    component: 'Image',
                    content: { url: 'image-url' },
                    props: undefined,
                  },
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: "# You're done" },
                      { component: 'Text', content: 'some content' },
                    ],
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'some-section-id',
              },
            ],
            type: 'meditation',
          },
        },
        {
          lang: {
            id: 'conversation-single-id-2',
            name: 'Test-single',
            root: 'meditation-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      { component: 'Text', content: '# Meditaiton Name' },
                      { component: 'Text', content: 'meditation description' },
                    ],
                  },
                  {
                    component: 'AudioPlayer',
                    content: { duration: undefined, uri: 'audioUri-value' },
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'meditation-section-id',
              },
            ],
            type: 'meditation',
          },
        },
        {
          lang: {
            id: 'conversation-single-id-3',
            name: 'Test-single',
            root: 'exercise-section-id',
            sections: [
              {
                content: [
                  {
                    component: 'TextGroup',
                    content: [
                      {
                        component: 'MiniBadge',
                        content: {
                          url: 'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
                        },
                      },
                      { component: 'Text', content: '# Exercise Name' },
                      { component: 'Text', content: 'exercise description' },
                    ],
                  },
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'ExerciseCompleteButton',
                        content: undefined,
                      },
                    ],
                  },
                ],
                id: 'exercise-section-id',
              },
            ],
            type: 'exercise',
          },
        },
      ]);
    });
  });

  describe('exercises', () => {
    it('removes sections and adds conversations', () => {
      const processedExercises = preprocessSingles({
        exercises,
        meditations,
      })(singles).exercises;

      expect(processedExercises).toEqual(
        expect.arrayContaining([
          {
            lang: expect.objectContaining({
              id: 'single-id-2',
              conversations: expect.arrayContaining([
                {
                  title: 'Test-single',
                  conversationId: 'conversation-single-id-2',
                },
              ]),
            }),
          },
        ]),
      );
    });
  });
});
