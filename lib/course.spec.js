import { preprocessCourses } from './course.js';

describe('preprocessCourses', () => {
  const lessons = [
    {
      lang: {
        id: 'lesson-id',
        name: 'Lesson Name',
        stations: [
          {
            id: 'conv1-id',
            sections: [
              {
                id: 'section-1',
                type: 'editorialWithImage',
                buttonText: 'next',
              },
              {
                id: 'section-2',
                type: 'editorialWithImage',
                buttonText: 'next',
              },
            ],
          },
          {
            id: 'conv2-id',
            sections: [
              {
                id: 'section-1',
                type: 'editorialWithImage',
                buttonText: 'next',
              },
              {
                id: 'section-2',
                type: 'editorialWithImage',
                buttonText: 'next',
              },
            ],
          },
        ],
      },
    },
  ];

  const courses = [
    {
      lang: {
        id: 'course-id-1',
        name: 'Test-course',
        lessonRefs: [
          {
            refId: 'lesson-id',
          },
        ],
      },
    },
  ];

  describe('conversations', () => {
    it('generates conversations', () => {
      expect(preprocessCourses({ lessons })(courses).conversations).toEqual([
        {
          lang: {
            id: 'conv1-id',
            lang: 'lang',
            root: 'section-1',
            sections: [
              {
                content: [
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'Button',
                        content: 'next',
                        conversationId: 'conv1-id',
                        sectionId: 'section-2',
                      },
                    ],
                  },
                ],
                id: 'section-1',
              },
              {
                content: [
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'NextConversationButton',
                        content: 'next',
                        conversationId: 'conv2-id',
                      },
                    ],
                  },
                ],
                id: 'section-2',
              },
            ],
          },
        },
        {
          lang: {
            id: 'conv2-id',
            lang: 'lang',
            root: 'section-1',
            sections: [
              {
                content: [
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'Button',
                        content: 'next',
                        conversationId: 'conv2-id',
                        sectionId: 'section-2',
                      },
                    ],
                  },
                ],
                id: 'section-1',
              },
              {
                content: [
                  {
                    component: 'ButtonGroup',
                    content: [
                      {
                        component: 'LessonCompleteButton',
                        content: 'next',
                        conversationId: 'conv2-id',
                        props: undefined,
                      },
                    ],
                  },
                ],
                id: 'section-2',
              },
            ],
          },
        },
      ]);
    });
  });

  describe('courses', () => {
    it('generates courses', () => {
      expect(preprocessCourses({ lessons })(courses).courses).toEqual([
        {
          lang: {
            id: 'course-id-1',
            lessons: [
              {
                conversations: [
                  { conversationId: 'conv1-id', title: undefined },
                  { conversationId: 'conv2-id', title: undefined },
                ],
                id: 'lesson-id',
                name: 'Lesson Name',
                stations: [
                  {
                    id: 'conv1-id',
                    sections: [
                      {
                        buttonText: 'next',
                        id: 'section-1',
                        type: 'editorialWithImage',
                      },
                      {
                        buttonText: 'next',
                        id: 'section-2',
                        type: 'editorialWithImage',
                      },
                    ],
                  },
                  {
                    id: 'conv2-id',
                    sections: [
                      {
                        buttonText: 'next',
                        id: 'section-1',
                        type: 'editorialWithImage',
                      },
                      {
                        buttonText: 'next',
                        id: 'section-2',
                        type: 'editorialWithImage',
                      },
                    ],
                  },
                ],
              },
            ],
            name: 'Test-course',
          },
        },
      ]);
    });
  });
});
