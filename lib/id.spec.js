/*
 * Copyright (c) 2018-2021 29k International AB
 */

import { v4 as uuidv4 } from 'uuid';

import {
  decode,
  encode,
  uuidToId,
  idToUuid,
  namespace,
  dateString,
  createCourseChannelId,
  createGroupSharingId,
  createRtcRoomId,
  createRtcUserId,
  createConversationIdFromLesson,
} from './id.js';

describe('decode', () => {
  const table = [
    ['XhdsiggaDgcW4Cq91QZmW2', 'f89ea2a2-9348-535c-9570-bf7548b85933'],
    ['L4WZBve2DR8g8PY8mLEosn', '9a5b146a-61b9-4aef-8519-90304320dcf1'],
    ['HFyi9XR6Y3gv9jHjcrMa5o', '83a9946b-af74-4186-8483-330f6d6fa74a'],
    ['4jEpdPdPWwdk4ujQZ4UoeW', '1e312c02-8c79-44a8-b1cc-b74554cf391f'],
    ['SFQRPuzwYWGDfMCK1DUkmt', 'cc77500f-bca0-48bd-a282-14842b25aa2f'],
    ['YcVfxkQb6JRzqk5kF2tNLv', 'ffffffff-ffff-ffff-ffff-ffffffffffff'],
    ['zzzzzzzzzzzzzzzzzzzzzz', '01d5b20a-d2d2-330b-10a7-bb82b9f63fffff'], // invalid
    ['1111111111111111', '00000000-0000-0000-0000-000000000000'],
  ];
  it.each(table)('decodes %s to %s', (id, uuid) => {
    const hex = uuid.replace(/-/g, '');
    expect(decode(id).toString('hex')).toEqual(hex);
  });

  it.each(table)('encodes %s from %s', (id, uuid) => {
    const hex = uuid.replace(/-/g, '');
    const bytes = Buffer.from(hex, 'hex');
    expect(encode(bytes)).toEqual(id);
  });

  it.each(table)('converts id %s from uuid %s', (id, uuid) => {
    expect(uuidToId(uuid)).toEqual(id);
  });

  it.each(table)('converts id %s to uuid %s', (id, uuid) => {
    expect(idToUuid(id)).toEqual(uuid);
  });
});

describe('namespace', () => {
  it('is valid uuid v4', () => {
    const string = '---<{{@ ¿ @)--}-';
    const expected = uuidv4({ random: Buffer.from(string) });
    expect(namespace).toEqual(expected);
  });
});

describe('dateString', () => {
  it.each([
    ['0', new Date('1970-01-01T00:00:00Z')],
    ['1', new Date('1970-01-01T00:00:01Z')],
    ['60', new Date('1970-01-01T00:01:00Z')],
    ['3600', new Date('1970-01-01T01:00:00Z')],
    ['86400', new Date('1970-01-02T00:00:00Z')],
    ['1589381778', new Date('2020-05-13T14:56:18Z')],
    ['1589381778', new Date('2020-05-13T14:56:18.250Z')],
    ['1589381778', new Date('2020-05-13T14:56:18.999Z')],
    ['1589381778', new Date('2020-05-13T16:56:18+02:00')],
    ['1589381779', new Date('2020-05-13T14:56:19Z')],
  ])('#%# generates id %s from %s', (expected, input) => {
    expect(dateString(input)).toEqual(expected);
  });
});

describe('createCourseChannelId', () => {
  it('generates expected value', () => {
    const courseId = '1e312c02-8c79-44a8-b1cc-b74554cf391f';
    const expected = 'XhdsiggaDgcW4Cq91QZmW2';
    expect(createCourseChannelId(courseId)).toEqual(expected);
  });
});

describe('createGroupSharingId', () => {
  const sharingAt1 = new Date('2020-05-13T15:00:00Z');
  const sharingAt2 = new Date('2020-05-13T15:00:01Z');
  it.each([
    ['3KJFDNUfHE5ku1dBJ6j8u2', 'group1', 'lesson1', sharingAt1],
    ['Uq7GspyNNmxizwYHzK5nMd', 'group2', 'lesson1', sharingAt1],
    ['U8ZwTi6Ss8jmt7ZiCgsimY', 'group1', 'lesson2', sharingAt1],
    ['FkFZtNon9dHpoFrLV8mb3z', 'group1', 'lesson1', sharingAt2],
  ])('generates %s from %s %s %s', (expected, groupId, lessonId, sharingAt) => {
    expect(createGroupSharingId(groupId, lessonId, sharingAt)).toEqual(
      expected,
    );
  });
});

describe('createConversationIdFromLesson', () => {
  it.each([
    ['7BAYGo6use5bBybehX6cJn', 'some-course-id', 'some-lesson-id'],
    ['LxA6zL38eWtTvqDgaetGA', 'some-other-course-id', 'some-other-lesson-id'],
    [
      'EiRy9KYDgNt9MCxjoFsFfF',
      '1e312c02-8c79-44a8-b1cc-b74554cf391f',
      'aa14fbdf-7244-493c-8eba-b82a4858bb16',
    ],
  ])('generates %s from %s %s %s', (expected, courseId, lessonId) => {
    expect(createConversationIdFromLesson(courseId, lessonId)).toEqual(
      expected,
    );
  });
});

describe('createRtcRoomId', () => {
  it.each([
    ['4uWYu2aV2skJ7Lh8tbQLm7', 'group1'],
    ['4PJYR8ch6jCPaNNsuATBhc', 'group2'],
  ])('generates %s from %s %s %s', (expected, groupId) => {
    expect(createRtcRoomId(groupId)).toEqual(expected);
  });
});

describe('createRtcUserId', () => {
  it.each([
    ['2F26MmUV4jRhupBk95FtiF', 'room1', 'user1'],
    ['Vw7iEUBRDT9gGwvuRfjFEK', 'room1', 'user2'],
    ['EJWv773R2NNL45SbjGBBnB', 'room2', 'user1'],
    ['8iCgtEk9uEijnhkp8tkpgS', 'room2', 'user2'],
  ])('generates %s from %s %s %s', (expected, roomId, userId) => {
    expect(createRtcUserId(roomId, userId)).toEqual(expected);
  });
});
