import { preprocessConversations } from './index.js';

jest.mock(
  './templates/sharingExercise',
  () => () => 'transformed sharingExercise mock',
);
jest.mock(
  './templates/microInterventionDay',
  () => () => 'transformed microInterventionDay mock',
);
jest.mock('./templates/audio', () => () => 'transformed audio mock');
jest.mock('./templates/video', () => () => 'transformed video mock');
jest.mock('./templates/writing', () => () => 'transformed writing mock');
jest.mock(
  './templates/editorialWithImage',
  () => () => 'transformed editorialWithImage mock',
);
jest.mock(
  './templates/exerciseRef',
  () => () => 'transformed exerciseRef mock',
);
jest.mock(
  './templates/meditationRef',
  () => () => 'transformed meditationRef mock',
);

describe('preprocessConversations', () => {
  it('does not transform regular sections', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                foo: 'bar',
              },
              {
                id: 'some-other-section-id',
                type: 'defaultSection',
                bar: 'baz',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: [
            {
              id: 'some-section-id',
              foo: 'bar',
            },
            {
              id: 'some-other-section-id',
              type: 'defaultSection',
              bar: 'baz',
            },
          ],
        },
      },
    ]);
  });

  it('supports sharingExercise', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'sharingExercise',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed sharingExercise mock'],
        },
      },
    ]);
  });

  it('supports microInterventionDay', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'microInterventionDay',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed microInterventionDay mock'],
        },
      },
    ]);
  });

  it('supports audio', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'audio',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed audio mock'],
        },
      },
    ]);
  });

  it('supports video', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'video',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed video mock'],
        },
      },
    ]);
  });

  it('supports writing', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'writing',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed writing mock'],
        },
      },
    ]);
  });

  it('supports meditationRef', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'meditation',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed meditationRef mock'],
        },
      },
    ]);
  });

  it('supports exerciseRef', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'exercise',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed exerciseRef mock'],
        },
      },
    ]);
  });

  it('supports editorialWithImage', () => {
    expect(
      preprocessConversations([
        {
          en: {
            id: 'some-conversation-id',
            sections: [
              {
                id: 'some-section-id',
                type: 'editorialWithImage',
              },
            ],
          },
        },
      ]),
    ).toEqual([
      {
        en: {
          id: 'some-conversation-id',
          sections: ['transformed editorialWithImage mock'],
        },
      },
    ]);
  });
});
