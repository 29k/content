import {
  AudioPlayer,
  Button,
  CloseButton,
  ButtonGroup,
  CheckPNsStatusButton,
  CompleteLessonButton,
  ExerciseCarousel,
  ExerciseReview,
  Heading,
  Image,
  Input,
  LessonCompleteButton,
  Pause,
  PushNotification,
  RequestPNsButton,
  Section,
  SubmitSharingButton,
  Subtitle,
  Text,
  TextGroup,
  TextParagraphs,
  VideoPlayer,
} from './index.js';

describe('TextGroup', () => {
  it('returns object', () => {
    expect(TextGroup('Some text', 'some other text')).toEqual({
      component: 'TextGroup',
      content: ['Some text', 'some other text'],
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(TextGroup()).toBe(undefined);
  });
});

describe('Text', () => {
  it('return object', () => {
    expect(Text('Some text')).toEqual({
      component: 'Text',
      content: 'Some text',
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(Text()).toBe(undefined);
  });
});

describe('TextParagraphs', () => {
  it('return object', () => {
    expect(TextParagraphs('Some\nlonger\ntext')).toEqual([
      { component: 'Text', content: 'Some\nlonger\ntext' },
    ]);
  });
});

describe('Heading', () => {
  it('return object', () => {
    expect(Heading('Some text')).toEqual({
      component: 'Text',
      content: '# Some text',
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(Heading()).toBe(undefined);
  });
});

describe('Image', () => {
  it('return object', () => {
    expect(Image('some://uri')).toEqual({
      component: 'Image',
      content: {
        url: 'some://uri',
      },
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(Image()).toBe(undefined);
  });
});

describe('VideoPlayer', () => {
  it('return object', () => {
    expect(
      VideoPlayer(
        'some://video-uri',
        'some://image-uri',
        'some://subtitle-uri',
        'A title',
        'The duration',
      ),
    ).toEqual({
      component: 'VideoStories',
      content: [
        {
          videoUri: 'some://video-uri',
          imageUri: 'some://image-uri',
          subtitleUri: 'some://subtitle-uri',
          title: 'A title',
          duration: 'The duration',
        },
      ],
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(VideoPlayer()).toBe(undefined);
  });
});

describe('AudioPlayer', () => {
  it('return object', () => {
    expect(AudioPlayer('some://uri', 'The duration')).toEqual({
      component: 'AudioPlayer',
      content: {
        uri: 'some://uri',
        duration: 'The duration',
      },
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(AudioPlayer()).toBe(undefined);
  });
});

describe('PushNotification', () => {
  it('returns object', () => {
    expect(PushNotification('title', 'body', 'when', 'target')).toEqual({
      options: {
        title: 'title',
        body: 'body',
        when: 'when',
        target: 'target',
      },
      type: 'PushNotification',
    });
  });
});

describe('Pause', () => {
  it('renders object', () => {
    expect(Pause('now', 'some target')).toEqual({
      type: 'Pause',
      options: {
        when: 'now',
        target: 'some target',
      },
    });
  });
});

describe('Subtitle', () => {
  it('returns object', () => {
    expect(Subtitle('some content')).toEqual({
      component: 'Subtitle',
      content: 'some content',
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(Subtitle()).toBe(undefined);
  });
});

describe('ButtonGroup', () => {
  it('returns object', () => {
    expect(ButtonGroup('some content')).toEqual({
      component: 'ButtonGroup',
      content: ['some content'],
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(ButtonGroup()).toBe(undefined);
  });
});

describe('Button', () => {
  it('returns object', () => {
    const params = { foo: 'bar' };

    expect(
      Button('some content', 'some conversationId', 'some sectionId', params),
    ).toEqual({
      component: 'Button',
      content: 'some content',
      conversationId: 'some conversationId',
      sectionId: 'some sectionId',
      params: { foo: 'bar' },
    });
  });

  it('returns undefined when no argument is passed', () => {
    expect(Button()).toBe(undefined);
  });
});

describe('CloseButton', () => {
  it('returns object', () => {
    expect(CloseButton('some content')).toEqual({
      component: 'CloseButton',
      content: 'some content',
    });
  });
});

describe('CheckPNsStatusButton', () => {
  it('returns object', () => {
    const params = {
      notDeterminedSectionId: 'some notDeterminedSectionId',
      deniedSectionId: 'some deniedSectionId',
    };
    expect(
      CheckPNsStatusButton(
        'some content',
        'some conversationId',
        'some sectionId',
        params,
      ),
    ).toEqual({
      component: 'CheckPNsStatusButton',
      content: 'some content',
      conversationId: 'some conversationId',
      sectionId: 'some sectionId',
      notDeterminedSectionId: 'some notDeterminedSectionId',
      deniedSectionId: 'some deniedSectionId',
    });
  });
});
describe('RequestPNsButton', () => {
  it('returns object', () => {
    const params = {
      deniedSectionId: 'some deniedSectionId',
    };
    expect(
      RequestPNsButton(
        'some content',
        'some conversationId',
        'some sectionId',
        params,
      ),
    ).toEqual({
      component: 'RequestPNsButton',
      content: 'some content',
      conversationId: 'some conversationId',
      sectionId: 'some sectionId',
      deniedSectionId: 'some deniedSectionId',
    });
  });
});

describe('Section', () => {
  it('returns object', () => {
    const content = ['some content'];
    const props = { foo: 'bar' };
    expect(Section('1', props)(content, 'some task')).toEqual({
      content: ['some content'],
      id: '1',
      props: { foo: 'bar' },
      task: 'some task',
    });
  });
});

describe('SubmitSharingButton', () => {
  it('returns object', () => {
    const data = {
      conversationId: 'some conversationId',
      sectionId: 'some sectionId',
      key: 'some key',
      category: 'some category',
      question: 'some question',
    };

    expect(SubmitSharingButton('some content', data)).toEqual({
      component: 'SubmitSharingButton',
      content: 'some content',
      conversationId: 'some conversationId',
      sectionId: 'some sectionId',
      props: {
        key: 'some key',
        category: 'some category',
        question: 'some question',
      },
    });
  });
});

describe('ExerciseCarousel', () => {
  it('returns object', () => {
    expect(ExerciseCarousel('some category')).toEqual({
      component: 'ExerciseCarousel',

      props: { category: 'some category' },
    });
  });
});

describe('LessonCompleteButton', () => {
  it('returns object', () => {
    const props = {
      isPrimary: true,
    };
    expect(
      LessonCompleteButton('some content', 'some conversationId', props),
    ).toEqual({
      component: 'LessonCompleteButton',
      content: 'some content',
      conversationId: 'some conversationId',
      props: {
        isPrimary: true,
      },
    });
  });
});

describe('CompleteLessonButton', () => {
  it('returns object', () => {
    expect(CompleteLessonButton('some content', 'some conversationId')).toEqual(
      {
        component: 'CompleteLessonButton',
        content: 'some content',
        conversationId: 'some conversationId',
      },
    );
  });
});

describe('Input', () => {
  it('returns object', () => {
    const data = {
      target: {
        conversationId: 'some conversationId',
        sectionId: 'some sectionId',
      },
      skipTarget: {
        conversationId: 'some conversationId',
        sectionId: 'skip sectionId',
      },
      placeholder: 'some placeholder',
      questionKey: 'some key',
      enableSkip: true,
      skipText: 'some text',
    };
    expect(Input(data)).toEqual({
      component: 'Input',
      props: {
        target: {
          conversationId: 'some conversationId',
          sectionId: 'some sectionId',
        },
        skipTarget: {
          conversationId: 'some conversationId',
          sectionId: 'skip sectionId',
        },
        placeholder: 'some placeholder',
        questionKey: 'some key',
        enableSkip: true,
        skipText: 'some text',
      },
    });
  });
});

describe('ExerciseReview', () => {
  it('returns object', () => {
    const data = { key: 'some key' };
    expect(ExerciseReview(data)).toEqual({
      component: 'ExerciseReview',
      props: {
        key: 'some key',
      },
    });
  });
});
