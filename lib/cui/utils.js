export const removeNilComponents = (...components) =>
  components.filter(Boolean);
