import {
  TextGroup,
  Heading,
  TextParagraphs,
  ButtonGroup,
  Section,
  VideoStories,
} from '../components/index.js';

import { removeNilComponents } from '../utils.js';

const video = (
  { sectionId },
  { title, description, buttons = [], videos = [] },
) => {
  return Section(sectionId)(
    removeNilComponents(
      TextGroup(
        ...removeNilComponents(Heading(title), ...TextParagraphs(description)),
      ),
      VideoStories(videos),
      ButtonGroup(...buttons),
    ),
  );
};

export default video;
