import writing from './writing.js';

describe('writing', () => {
  it('returns an object', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
    };
    const data = {
      title: 'A title',
      time: '1 light year',
      question: 'The description',
      shortDescription: 'The shorter description',
      longDescription: 'The longer description',
      buttons: [
        {
          component: 'Button',
          content: 'Go go go',
          conversationId: 'some-conversation-id',
          sectionId: 'u-turn',
        },
      ],
    };

    expect(writing(parent, data)).toEqual({
      id: 'some-section-id',
      content: [
        {
          component: 'Badge',
          content: {
            url: 'https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1617023623/Badges/exercise_badge_ltbfuq.png',
          },
        },
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: '# A title' },
            { component: 'Subtitle', content: '1 light year' },
            { component: 'Text', content: 'The description' },
            { component: 'Text', content: 'The shorter description' },
            { component: 'Text', content: 'The longer description' },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Go go go',
              conversationId: 'some-conversation-id',
              sectionId: 'u-turn',
            },
          ],
        },
      ],
    });
  });
});
