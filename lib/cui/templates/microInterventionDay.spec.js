import { TextGroup, Text } from '../components/index.js';
import microInterventionDay from './microInterventionDay.js';

beforeEach(() => jest.clearAllMocks());

jest.mock('./questCheckIn', () => ({
  sectionIDs: {
    FOLLOW_UP: 'followUp',
    Q1: 'q1',
  },
  template: jest.fn(() => []),
}));

jest.mock('./sharingExercise', () => jest.fn(() => []));

jest.mock('../../../src/ui/CUI.MicroInterventionDay.json', () => ({
  en: {
    welcome: {
      title__markdown: '# Welcome to ALIVE daily quests',
      quest__markdown:
        'In this course you will be given daily quests inspiring you to do things in a different way than you usually do them.',
      checkIn__markdown:
        'By the end of the day you’ll get a chance to check-in and reflect back on how it went.',
      groupSharing__markdown:
        'Once a week we recommend you to share your experiences with others in a live video sharing.',
      start__button: 'Let’s begin',
    },
    allowPNs: {
      title__markdown: 'One more thing!',
      text__markdown:
        'To receive the quests and reminders for check-ins, we need to be able to send you push notifications.',
      enablePNs__button: 'Sure, send me notifications',
    },
    PNsDenied: {
      title__markdown: '# Uh oh!',
      paragraph1__markdown: 'You’ve disabled push notifications.',
      paragraph2__markdown:
        'In order to be able to participate and benefit from this course you need to have push notifications enabled.',
      paragraph3__markdown:
        'Please go to your notification settings and enable them manually from there.',
      PNsTurnedOn__button: 'I’ve turned it on',
    },
    scheduleTask: {
      titleInitialDay__markdown: '# You’re all set and ready to go.',
      title__markdown: '# Next quest ahead',
      textInitialDay__markdown:
        'We’ll notify you about your first quest in the morning.',
      text__markdown: 'We’ll notify you about your next quest in the morning.',
      confirm__button: 'OK',
    },
    task: {
      title__markdown: '# Today’s quest',
      guiding__markdown:
        'By the end of the day, you’ll get a chance to check-in and reflect back on how it went.',
      confirm__button: 'I’m on it',
    },
    skip: {
      text__markdown: 'No problem, you will get another quest tomorrow.',
      confirm__button: 'OK',
    },
    endOfDay: {
      title__markdown: '## That’s it for today.',
      paragraph__markdown: 'You will get your next quest in the morning.',
      confirm__button: 'OK',
    },
    endOfWeek: {
      title__markdown: '# You did it',
      text__markdown: 'That’s the end of this week’s quests.',
      weekCompleted__button: 'OK',
    },
  },
}));

describe('microInterventionDay', () => {
  const parent = {
    conversationId: 'some-conversation-id',
    lang: 'en',
  };
  const data = {
    nextConversationId: 'some-next-conversation-id',
    initialDay: true,
    lessonlastDay: false,
    task: {
      title: 'title',
      body: 'body',
      days: 1,
      hours: 0,
      minutes: 0,
    },
    followUp: {
      title: 'title',
      body: 'body',
      days: 1,
      hours: 0,
      minutes: 0,
    },
    sharingWall: {
      question: 'Sharing wall question',
      exampleText: 'Sharing wall example text',
    },
  };

  let result;

  beforeAll(() => {
    result = microInterventionDay(parent, data);
  });

  it('returns 0', () => {
    expect(result[0]).toEqual({
      id: 'welcome',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content: expect.any(String),
            },
            { component: 'MiniBadge', content: { url: expect.any(String) } },
            {
              component: 'Text',
              content: expect.any(String),
            },
            { component: 'MiniBadge', content: { url: expect.any(String) } },
            {
              component: 'Text',
              content: expect.any(String),
            },
            { component: 'MiniBadge', content: { url: expect.any(String) } },
            {
              component: 'Text',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CheckPNsStatusButton',
              content: 'Let’s begin',
              conversationId: 'some-conversation-id',
              sectionId: 'scheduleTask',
              notDeterminedSectionId: 'allowPNs',
              deniedSectionId: 'PNsDenied',
            },
          ],
        },
      ],
    });
  });

  it('returns 1', () => {
    expect(result[1]).toEqual({
      id: 'allowPNs',
      content: [
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: 'One more thing!' },
            {
              component: 'Text',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'RequestPNsButton',
              content: 'Sure, send me notifications',
              conversationId: 'some-conversation-id',
              sectionId: 'scheduleTask',
              deniedSectionId: 'PNsDenied',
            },
          ],
        },
      ],
    });
  });

  it('returns 2', () => {
    expect(result[2]).toEqual({
      id: 'PNsDenied',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content: expect.any(String),
            },
            {
              component: 'Text',
              content: expect.any(String),
            },
            {
              component: 'Text',
              content: expect.any(String),
            },
            {
              component: 'Text',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CheckPNsStatusButton',
              content: 'I’ve turned it on',
              conversationId: 'some-conversation-id',
              sectionId: 'scheduleTask',
              notDeterminedSectionId: 'allowPNs',
              deniedSectionId: 'PNsDenied',
            },
          ],
        },
      ],
    });
  });

  it('returns 3', () => {
    expect(result[3]).toEqual({
      id: 'scheduleTask',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content: expect.any(String),
            },
            {
              component: 'Text',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CloseButton',
              content: 'OK',
            },
          ],
        },
      ],
      task: {
        type: 'PushNotification',
        options: {
          when: { days: 1, hours: 0, minutes: 0 },
          title: 'title',
          body: 'body',
          target: {
            conversationId: 'some-conversation-id',
            sectionId: 'task',
          },
        },
      },
    });
  });

  it('returns 4', () => {
    expect(result[4]).toEqual({
      id: 'task',
      params: {
        fillViewport: true,
      },
      content: [
        {
          component: 'Badge',
          content: {
            url: expect.any(String),
          },
        },
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: expect.any(String) },
            { component: 'Text', content: 'body' },
            { component: 'MiniBadge', content: { url: expect.any(String) } },
            {
              component: 'Guiding',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CloseButton',
              content: 'I’m on it',
            },
          ],
        },
      ],
      task: {
        type: 'PushNotification',
        options: {
          when: { days: 1, hours: 0, minutes: 0 },
          title: 'title',
          body: 'body',
          target: {
            conversationId: 'some-conversation-id',
            sectionId: 'followUp',
          },
        },
      },
    });
  });

  // ... Skippnig sections questCheckIn.spec.js
  // ... Skipping sections sharingExercise.spec.js

  it('returns 5', () => {
    expect(result[5]).toEqual({
      id: 'endOfWeek',
      content: [
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: '# You did it' },
            {
              component: 'Text',
              content: expect.any(String),
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'LessonCompleteButton',
              content: 'OK',
              conversationId: 'some-conversation-id',
            },
          ],
        },
      ],
    });
  });

  it('returns 6', () => {
    expect(result[6]).toEqual({
      id: 'endOfDay',
      content: [
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: expect.any(String) },
            { component: 'Text', content: expect.any(String) },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CloseButton',
              content: 'OK',
            },
          ],
        },
      ],
    });
  });

  it('returns 7', () => {
    expect(result[7]).toEqual({
      id: 'skip',
      content: [
        TextGroup(Text(expect.any(String))),
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'CloseButton',
              content: 'OK',
            },
          ],
        },
      ],
    });
  });
});
