import {
  Section,
  AudioPlayer,
  TextParagraphs,
  ButtonGroup,
  Heading,
  TextGroup,
} from '../components/index.js';
import { removeNilComponents } from '../utils.js';

export default ({ sectionId }, { refId, ...section }, meditations = {}) => {
  const meditation = meditations[refId];
  if (!meditation) {
    return [];
  }

  return Section(sectionId)(
    removeNilComponents(
      TextGroup(
        ...removeNilComponents(
          Heading(meditation.name),
          ...TextParagraphs(meditation.description),
        ),
      ),
      AudioPlayer(meditation.audioUri),
      ButtonGroup(...section.buttons),
    ),
  );
};
