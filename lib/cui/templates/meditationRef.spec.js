import meditationRef from './meditationRef.js';

describe('meditationRef', () => {
  it('returns an object', () => {
    const parent = {
      sectionId: 'some-section-id',
    };
    const meditations = {
      'meditation-id': {
        name: 'name-value',
        description: 'description-value',
        audioUri: 'audioUri-value',
        duration: 'duration-value',
      },
    };
    const data = {
      refId: 'meditation-id',
      buttons: [
        {
          component: 'Button',
          content: 'Go go go',
          conversationId: 'some-conversation-id',
          sectionId: 'u-turn',
        },
      ],
    };

    expect(meditationRef(parent, data, meditations)).toEqual({
      id: 'some-section-id',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content: '# name-value',
            },
            {
              component: 'Text',
              content: 'description-value',
            },
          ],
        },
        {
          component: 'AudioPlayer',
          content: {
            uri: 'audioUri-value',
            duration: undefined,
          },
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Go go go',
              conversationId: 'some-conversation-id',
              sectionId: 'u-turn',
            },
          ],
        },
      ],
    });
  });
});
