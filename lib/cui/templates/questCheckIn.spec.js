import { template } from './questCheckIn.js';

jest.mock('../../../src/ui/CUI.QuestCheckIn.json', () => ({
  'some-lang': {
    followUp7: {
      answer1__button: 'Never True',
      answer2__button: 'Very Seldom True',
      answer3__button: 'Seldom True',
      answer4__button: 'Sometimes True',
      answer5__button: 'Frequently True',
      answer6__button: 'Almost Always True',
      answer7__button: 'Always True',
    },
    followUp3: {
      answer1__button: 'Not at all true',
      answer2__button: 'Somewhat true',
      answer3__button: 'Entirely true',
    },
    followUp: {
      title__markdown: '# Time to check-in',
      question: 'Did you do it?',
      info__markdown: 'Today’s quest was:',
      taskCompleted__button: 'Yes',
      taskNotCompleted__button: 'No',
      startTest__button: 'Start test',
    },
    followUpComplete: {
      title__markdown: '# Well done',
      paragraph1__markdown:
        '*Please note that during this test period the above answers won’t be stored.*',
      paragraph2__markdown:
        '*Still, checking in is a great chance for you to reflect and integrate what you’ve learned from today’s quest.*',
      complete__button: 'I understand',
    },
    q1: {
      paragraph1__markdown:
        'Great! Let’s talk about how today’s quest made you feel.',
      paragraph2__markdown:
        'Based upon your own experiences, please rate how true the following statements are to you.',
      question__markdown: '~~I am proud of the way I undertook this quest.~~',
    },
    q2: {
      initialDayQuestion__markdown: '~~I found this quest difficult to do.~~',
      question__markdown:
        '~~I found this quest less difficult to do than previous quests.~~',
    },
    q3: {
      initialDayQuestion__markdown:
        '~~I have felt comfortable with how I’ve been living my life.~~',
      question__markdown:
        'Since last check-in, ~~I have felt comfortable with how I’ve been living my life.~~',
    },
    q4: {
      initialDayQuestion__markdown:
        '~~I have felt I’m looking forward to what the day will bring.~~',
      question__markdown:
        'Since last check-in, ~~I have felt I’m looking forward to what the day will bring.~~',
    },
    q5: {
      initialDayQuestion__markdown: '~~I have felt isolated.~~',
      question__markdown: 'Since last check-in, ~~I have felt isolated.~~',
    },
    q6: {
      initialDayQuestion__markdown: '~~I have felt unable to be myself.~~',
      question__markdown:
        'Since last check-in, ~~I have felt unable to be myself.~~',
    },
    q7: {
      initialDayQuestion__markdown: '~~I have felt active and vigorous.~~',
      question__markdown:
        'Since last check-in, ~~I have felt active and vigorous.~~',
    },
    q8: {
      initialDayQuestion__markdown:
        '~~I have felt that I could be more open in what I say and do.~~',
      question__markdown:
        'Since last check-in, ~~I have felt that I could be more open in what I say and do.~~',
    },
  },
}));

describe('questCheckIn', () => {
  const parent = {
    conversationId: 'some-conversation-id',
    lang: 'some-lang',
  };
  const data = {
    nextConversationId: 'some-next-conversation-id',
    initialDay: true,
    task: {
      title: 'title',
      body: 'body',
      days: 1,
      hours: 0,
      minutes: 0,
    },
    followUp: {
      title: 'title',
      body: 'body',
      days: 1,
      hours: 0,
      minutes: 0,
    },
    questCompleteSectionId: 'endOfDay',
    skipFollowUpSectionId: 'skip',
    // autoProgress: autoProgress,
    taskCompletedSectionId: 'sharingWall',
  };

  let result;

  beforeAll(() => {
    result = template(parent, data);
  });

  it('returns 0', () => {
    expect(result[0]).toEqual({
      id: 'followUp',
      params: {
        fillViewport: true,
      },
      content: [
        {
          component: 'Badge',
          content: {
            url: expect.any(String),
          },
        },
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: expect.any(String) },
            { component: 'MiniBadge', content: { url: expect.any(String) } },
            { component: 'Text', content: expect.any(String) },
            { component: 'Guiding', content: 'body' },
            { component: 'Text', content: expect.any(String) },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: expect.any(String),
              conversationId: 'some-conversation-id',
              sectionId: 'sharingWall',
            },
            {
              component: 'Button',
              content: expect.any(String),
              conversationId: 'some-conversation-id',
              sectionId: 'skip',
            },
          ],
        },
      ],
    });
  });

  it('returns 1', () => {
    expect(result[1]).toEqual({
      id: 'q1',
      params: {
        fillViewport: true,
      },
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content:
                'Great! Let’s talk about how today’s quest made you feel.',
            },
            {
              component: 'Text',
              content:
                'Based upon your own experiences, please rate how true the following statements are to you.',
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Start test',
              conversationId: 'some-conversation-id',
              sectionId: 'q2',
            },
          ],
        },
      ],
    });
  });

  it('returns questionnaire set', () => {
    expect(result[2]).toEqual({
      id: 'q2',
      content: [
        {
          component: 'QuestionnaireSet',
          next: {
            sectionId: 'followUpComplete',
          },
          sets: [
            {
              options: [
                {
                  label: 'Not at all true',
                  score: 0,
                },
                {
                  label: 'Somewhat true',
                  score: 1,
                },
                {
                  label: 'Entirely true',
                  score: 2,
                },
              ],
              questions: [
                '~~I am proud of the way I undertook this quest.~~',
                '~~I found this quest difficult to do.~~',
                '~~I have felt comfortable with how I’ve been living my life.~~',
              ],
            },
            {
              options: [
                {
                  label: 'Never True',
                  score: 0,
                },
                {
                  label: 'Very Seldom True',
                  score: 1,
                },
                {
                  label: 'Seldom True',
                  score: 2,
                },
                {
                  label: 'Sometimes True',
                  score: 3,
                },
                {
                  label: 'Frequently True',
                  score: 4,
                },
                {
                  label: 'Almost Always True',
                  score: 5,
                },
                {
                  label: 'Always True',
                  score: 6,
                },
              ],
              questions: [
                '~~I have felt I’m looking forward to what the day will bring.~~',
                '~~I have felt isolated.~~',
                '~~I have felt unable to be myself.~~',
                '~~I have felt active and vigorous.~~',
                '~~I have felt that I could be more open in what I say and do.~~',
              ],
            },
          ],
        },
      ],
    });
  });

  it('returns followUpComplete', () => {
    expect(result[3]).toEqual({
      id: 'followUpComplete',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'Text',
              content: '# Well done',
            },
            {
              component: 'Text',
              content:
                '*Please note that during this test period the above answers won’t be stored.*',
            },
            {
              component: 'Text',
              content:
                '*Still, checking in is a great chance for you to reflect and integrate what you’ve learned from today’s quest.*',
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'I understand',
              conversationId: 'some-conversation-id',
              sectionId: 'endOfDay',
            },
          ],
        },
      ],
    });
  });
});
