import video from './video.js';

describe('video', () => {
  it('returns an object', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
    };
    const data = {
      title: 'A title',
      description: 'The description',
      videos: [
        {
          videoUri: 'youtube://cats-in-party-hats',
          imageUri: 'youtube://cats-in-party-hats-2',
        },
      ],
      buttons: [
        {
          component: 'Button',
          content: 'Go go go',
          conversationId: 'some-conversation-id',
          sectionId: 'u-turn',
        },
      ],
    };

    expect(video(parent, data)).toEqual({
      content: [
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: '# A title' },
            { component: 'Text', content: 'The description' },
          ],
        },
        {
          component: 'VideoStories',
          content: [
            {
              duration: undefined,
              imageUri: 'youtube://cats-in-party-hats-2',
              videoUri: 'youtube://cats-in-party-hats',
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Go go go',
              conversationId: 'some-conversation-id',
              sectionId: 'u-turn',
            },
          ],
        },
      ],
      id: 'some-section-id',
    });
  });
});
