import createRequire from '../../../lib/require.cjs';
import { removeNilComponents } from '../utils.js';
import {
  TextGroup,
  Text,
  ButtonGroup,
  Button,
  Section,
  SubmitSharingButton,
  ExerciseCarousel,
  ExerciseReview,
  Subtitle,
  Input,
  CompleteLessonButton,
} from '../components/index.js';

const cjsRequire = createRequire(import.meta.url);
const translations = cjsRequire('../../../src/ui/CUI.SharingExercise.json');

// These are common keys that allow us to identify parts of
// exercises in Amplitude
const parts = {
  START: 'start',
  SHARING_CHOICE: 'sharingChoice',
  DID_SHARE: 'didShare',
  DID_NOT_SHARE: 'didNotShare',
  DID_SKIP_SHARE: 'didSkipShare',
};

const prefixSectionIDs = (prefix) => ({
  START: prefix,
  SHARING_CHOICE: `${prefix}-${parts.SHARING_CHOICE}`,
  DID_SHARE: `${prefix}-${parts.DID_SHARE}`,
  DID_NOT_SHARE: `${prefix}-${parts.DID_NOT_SHARE}`,
  DID_SKIP_SHARE: `${prefix}-${parts.DID_SKIP_SHARE}`,
});

const sharingExerciseTemplate = (parent, data) => {
  const { conversationId, sectionId, lang } = parent;
  const {
    question,
    exampleText,
    nextConversation,
    nextSection,
    categoryKey = `${conversationId}-${sectionId}`,
    buttonText,
    enableEarlySkip = true,
  } = data;

  const sectionIDs = prefixSectionIDs(sectionId);
  const t = translations[lang];
  const questionKey = `${conversationId}-${sectionId}-sharingExercise`;

  const SectionPart = (id) => Section(id);

  const EndButton = (content) =>
    nextSection || nextConversation
      ? Button(content, nextConversation || conversationId, nextSection)
      : CompleteLessonButton(content, conversationId);

  return [
    SectionPart(
      sectionIDs.START,
      parts.START,
    )([
      ...removeNilComponents(
        TextGroup(
          ...removeNilComponents(
            Subtitle(t.sharingWall.title),
            Text(`## ${question}`),
          ),
        ),
        Input({
          target: {
            conversationId,
            sectionId: sectionIDs.SHARING_CHOICE,
          },
          skipTarget: {
            conversationId,
            sectionId: sectionIDs.DID_SKIP_SHARE,
          },
          skipText: t.sharingWall.skip__button,
          placeholder: t.exerciseInput.input_placeholder,
          questionKey,
          enableSkip: enableEarlySkip,
        }),
      ),
    ]),

    SectionPart(
      sectionIDs.SHARING_CHOICE,
      parts.SHARING_CHOICE,
    )([
      TextGroup(Text(t.sharingWall.body__markdown)),
      ExerciseCarousel(categoryKey, exampleText),
      TextGroup(Text(t.exerciseSharingChoice.body__markdown)),
      ButtonGroup(
        SubmitSharingButton(t.exerciseSharingChoice.yes__button, {
          conversationId,
          sectionId: sectionIDs.DID_SHARE,
          key: questionKey,
          category: categoryKey,
          question,
        }),
        Button(
          t.exerciseSharingChoice.no__button,
          conversationId,
          sectionIDs.DID_NOT_SHARE,
        ),
      ),
    ]),

    SectionPart(
      sectionIDs.DID_SHARE,
      parts.DID_SHARE,
    )([
      TextGroup(
        Text(t.exerciseShared.heading__markdown),
        Text(t.exerciseShared.body__markdown),
      ),
      ExerciseReview({ key: questionKey }),
      ButtonGroup(EndButton(buttonText || t.exerciseShared.continue__button)),
    ]),

    SectionPart(
      sectionIDs.DID_NOT_SHARE,
      parts.DID_NOT_SHARE,
    )([
      TextGroup(
        Text(t.exerciseNotShared.heading__markdown),
        Text(t.exerciseNotShared.body__markdown),
      ),
      ButtonGroup(
        EndButton(buttonText || t.exerciseNotShared.continue__button),
      ),
    ]),
    SectionPart(
      sectionIDs.DID_SKIP_SHARE,
      parts.DID_SKIP_SHARE,
    )([
      TextGroup(Text(t.exerciseSkipped.body__markdown)),
      ExerciseCarousel(categoryKey, exampleText),
      ButtonGroup(EndButton(buttonText || t.exerciseSkipped.continue__button)),
    ]),
  ];
};

export default sharingExerciseTemplate;
