import {
  applySpec,
  map,
  mapObjIndexed,
  prop,
  chain,
  groupBy,
  pipe,
  values,
  addIndex,
  toPairs,
  reduce,
} from 'ramda';
import { preprocessSection, TEMPLATES } from './cui/index.js';
import {
  Button,
  LessonCompleteButton,
  NextConversationButton,
} from './cui/components/index.js';
import { getContentLibForLang } from './utils.js';

const mapIndex = addIndex(map);
const groupByLang = reduce((acc, el) => ({ ...acc, [el.lang]: el }), {});

const connectSections = (convId, nextConvId) => (section, idx, sections) => {
  const isLastSection = sections.length === idx + 1;
  const nextSectionId = sections[idx + 1] && sections[idx + 1].id;

  switch (section.type) {
    case TEMPLATES.SHARING_EXERCISE:
      return {
        ...section,
        nextSection: !isLastSection && nextSectionId,
      };

    case TEMPLATES.EXERCISE:
    case TEMPLATES.MEDITATION:
    case TEMPLATES.EDITORIAL_WITH_IMAGE:
    case TEMPLATES.WRITING:
    case TEMPLATES.AUDIO:
    case TEMPLATES.VIDEO:
      return {
        ...section,
        buttons: [
          isLastSection && !nextConvId
            ? LessonCompleteButton(section.buttonText, convId)
            : isLastSection
            ? NextConversationButton(section.buttonText, nextConvId)
            : Button(section.buttonText, convId, nextSectionId),
        ],
      };
    default:
      return section;
  }
};

const stationToConversation =
  (contentLib = {}, lang) =>
  (station, idx, stations) => {
    const nextConversationId = stations[idx + 1] && stations[idx + 1].id;
    const { sections = [] } = station;

    return {
      ...station,
      root: sections[0].id,
      sections: pipe(
        mapIndex(connectSections(station.id, nextConversationId)),
        chain(preprocessSection(lang, station.id, contentLib)),
      )(sections),
    };
  };

const lessonToConversations =
  (contentLib = {}, lang) =>
  ({ refId }) => {
    const { stations = [] } = contentLib.lessons[refId] || {};
    const res = stations.map((station, idx) => ({
      ...stationToConversation(contentLib, lang)(station, idx, stations),
      lang,
    }));

    return res;
  };

// {en: course1, sv: course1}
// -> [{id: conv1, lang: en}, {id: conv1, lang: sv}]
const courseToConversations = (contentLib = {}) =>
  pipe(
    toPairs,
    chain(([lang, course]) => {
      const contentLibForLang = getContentLibForLang(lang, contentLib);
      const { lessonRefs = [] } = course;
      return chain(lessonToConversations(contentLibForLang, lang), lessonRefs);
    }),
    groupBy(prop('id')), // { 'conv1': [{conv1, en}, {conv1, sv}] }
    map(groupByLang), // { 'conv1': { en: conv1, sv: conv1 }, 'conv2': ... }
    values, // [{en: conv1, sv: conv1}, ...]
  );

const buildConversationRef = ({ id, name }) => ({
  title: name,
  conversationId: id,
});

const buildLesson = (lessons, contentLib) =>
  lessons.map(({ refId }) => {
    const lesson = contentLib.lessons[refId];
    if (!lesson) return;

    return {
      ...lesson,
      conversations: lesson.stations.map(buildConversationRef),
    };
  });

const buildCourse = (contentLib = {}) =>
  mapObjIndexed(({ lessonRefs = [], ...course }, lang) => {
    if (!lessonRefs.length) return course;

    const contentLibForLang = getContentLibForLang(lang, contentLib);
    return {
      ...course,
      lessons: buildLesson(lessonRefs, contentLibForLang),
    };
  });

const preprocessCourses = (contentLib) =>
  applySpec({
    conversations: chain(courseToConversations(contentLib)),
    courses: map(buildCourse(contentLib)),
  });

export { preprocessCourses };
