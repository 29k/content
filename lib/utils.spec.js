import { getContentLibForLang } from './utils.js';

describe('getContentLibForLang', () => {
  const contentLib = {
    meditations: [
      {
        en: {
          id: 'en-meditation',
          foo: 'bar',
        },
      },
    ],
    exercises: [
      {
        en: {
          id: 'en-exercise',
          bar: 'baz',
        },
      },
    ],
    lessons: [
      {
        en: {
          id: 'en-lesson',
          baz: 'ouff',
        },
      },
    ],
  };

  it('should extract lang-specific content', () => {
    expect(getContentLibForLang('en', contentLib)).toEqual({
      meditations: {
        'en-meditation': {
          id: 'en-meditation',
          foo: 'bar',
        },
      },
      exercises: {
        'en-exercise': {
          id: 'en-exercise',
          bar: 'baz',
        },
      },
      lessons: {
        'en-lesson': {
          id: 'en-lesson',
          baz: 'ouff',
        },
      },
    });
  });
});
