/**
 * At the moment we use babel in Jest until ESM is fully supported:
 * https://github.com/facebook/jest/issues/9430
 * https://github.com/facebook/jest/pull/10976
 */
module.exports = {
  env: {
    test: {
      presets: [['@babel/preset-env', { targets: { node: 'current' } }]],
      plugins: ['babel-plugin-transform-import-meta'],
    },
  },
};
