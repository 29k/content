{
  "en": {
    "id": "MKHk4pfvEUJ3tQ8wvHtZFG-2",
    "alias": "Self compassion - Gratitude 2",
    "type": "lesson",
    "root": "section2",
    "sections": [
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "What are you grateful for?",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/Rachel-studio-2.jpg",
        "title": "What are you grateful for?",
        "content": "Life can fly by without us even noticing. With all of our to-do’s, distractions and choices, we sometimes miss out on pausing to see what brings our lives meaning.\n\nSometimes all we need is a moment to put in words what we truly cherish. To recapture meaning in this present moment, here and now.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "video",
        "alias": "Rachel’s gratitude experience",
        "title": "Rachel’s gratitude experience",
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Continue",
            "conversationId": "MKHk4pfvEUJ3tQ8wvHtZFG-3"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/yogagirl_gratefulness_exercise.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622463087/Self%20Compassion/Gratitude_exercise1_zhxv2u.mp4"
          }
        ]
      }
    ]
  },
  "sv": {
    "id": "MKHk4pfvEUJ3tQ8wvHtZFG-2",
    "alias": "Self compassion - Gratitude 2",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Tacksamhet",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/Rachel-beach.jpg",
        "title": "Tacksamhet",
        "content": "I den här lektionen kommer du att reflektera över vad du är tacksam för i ditt liv, i dig själv och i dem du värnar om.\n\n### Forskningen\n\nDen här lektionen är inspirerad av den evidensbaserade övningen ”The Gratitude Visit” av Seligman, M. E. et al.\n\n### Dagens lektion består av tre delar:\n\n* Ett tacksamhetsbrev till någon du uppskattar (Skrivande)\n* Meditation\n* Tacksamhetsbrev till dig själv (Skrivande)\n\n### Förberedelse för lektionen:\n\n* 40 min ostörd tid\n* Penna och papper",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Vad är du tacksam för?",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/Rachel-studio-2.jpg",
        "title": "Vad är du tacksam för?",
        "content": "Livet kan flyga utan att vi ens märker det. Med alla våra att göra, distraktioner och val missar vi ibland att pausa för att se vad som ger våra liv mening.\n\nIbland är allt vi behöver en stund att sätta ord på det som vi verkligen värnar om. Att återfå mening i detta nuvarande ögonblick, här och nu.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "video",
        "alias": "Tacksamhetsbrev till någon du uppskattar",
        "title": "Tacksamhetsbrev till någon du uppskattar",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section4"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/yogagirl_gratefulness_exercise.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622463087/Self%20Compassion/Gratitude_exercise1_zhxv2u.mp4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "writing",
        "alias": "Ett tacksamhetsbrev till någon du uppskattar",
        "title": "Ett tacksamhetsbrev till någon du uppskattar",
        "time": "Ta 10-20 min",
        "shortDescription": "Välj en person i ditt liv som du uppskattar, men har inte haft en chans att uttrycka din tacksamhet gentemot.\n\nTa en stund att reflektera över vad den här personen har gjort som har gjort att du känner dig tacksam. Skriv sedan ett brev till dem och uttryck din tacksamhet för vad den här personen har väckt ditt liv.\n\nSkriv detta brev som om du skulle lämna det personligen till dem, till exempel kanske du skulle börja med \"Kära ...\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "audio",
        "title": "Meditation",
        "description": "Bli nu närvarande med vad som händer i dig genom denna korta meditation.",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/yg_self_compassion/inner_critic/yogagirl_inner-critic_meditation.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "editorialWithImage",
        "alias": "Vad händer om vi kan uppskatta oss själva på samma sätt som vi uppskattar andra?",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/gratefulness/Rachel-studio.jpg",
        "title": "Vad händer om vi kan uppskatta oss själva på samma sätt som vi uppskattar andra?",
        "content": "De flesta människor kan se nyttan av att uttrycka tacksamhet mot andra, det växer och stärker vår relation med dem. Men vi uttrycker sällan tacksamhet inåt och därmed missar vi dessa fördelar för oss själva.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section7"
          }
        ]
      },
      {
        "id": "section7",
        "type": "writing",
        "alias": "Ett tacksamhetsbrev till dig själv",
        "title": "Ett tacksamhetsbrev till dig själv",
        "time": "Ta 10-20 min",
        "shortDescription": "Låt oss nu vända de tacksamma ögonen inåt. Börja med att välja attribut för dig själv som du uppskattar men inte har haft en chans att uttrycka din tacksamhet för.\n\nReflektera nu över dina prestationer och val du har gjort i ditt liv som du är tacksam för.\n\nNu när du har reflekterat över dessa områden, skriv ett tacksägelsebrev till dig själv ur detta medkännande, tacksamma perspektiv.\n",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "Bra gjort!",
        "title": "Bra gjort!",
        "content": "Återanslutning med tacksamhet kan kännas kraftfull, sårbar och skrämmande. Nästa stora steg är ännu kraftfullare.\n\n### Låt dem veta att du är tacksam\n\nOm det är möjligt är din nästa utmaning att uttrycka din tacksamhet mot personen du skrev tacksamhetsbrevet till.\n\nVi rekommenderar att du träffar dem personligen (eller det näst bästa) och läser ditt brev högt för dem.\n\nDetta är en mycket kraftfull upplevelse.\n\n",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section9"
          }
        ]
      },
      {
        "id": "section9",
        "type": "editorialWithImage",
        "alias": "# Bra ~~jobbat!~~",
        "title": "# Bra ~~jobbat!~~",
        "content": "Bra gjort när du har slutfört ** Tacksamhet **, du har precis gjort en underbar sak för dig själv.\n\nOm du väljer att delta i en delning med din grupp behöver du följande övningar nära till hands:\n\n* Ett tacksamhetsbrev till någon du uppskattar\n* Ett tacksamhetsbrev till dig själv\n",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Stäng"
          }
        ]
      }
    ]
  },
  "pt": {
    "id": "MKHk4pfvEUJ3tQ8wvHtZFG-2",
    "alias": "Self compassion - Gratitude 2",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Gratidão",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448860/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L6P1gratitudeautocompaixao_sebxgj.jpg",
        "title": "Gratidão",
        "content": "Nesta sessão, vais refletir sobre as coisas pelas quais deves agradecer na tua vida, em relação a ti e àqueles de quem gostas.\n\n### A investigação\n\nEsta sessão é inspirada no exercício de M. E. Seligman et al. \"A visita de agradecimento\".\n\n### A sessão de hoje consiste em três partes:\n\n* Uma carta de agradecimento a alguém que valorizas (Escrita)\n* Meditação\n* Uma carta de agradecimento a ti (Escrita)\n\n### Preparação para a sessão:\n\n* 40 min de tempo sem perturbações\n* Papel e caneta",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "O que agradeces?",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448861/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L6P2gratitudeautocompaixao_ztv3yj.jpg",
        "title": "O que agradeces?",
        "content": "A vida pode voar sem sequer nos darmos conta. Com todos os nossos afazeres, distrações e escolhas, muitas vezes não fazemos uma pausa para perceber o que dá sentido às nossas vidas.\n\nPor vezes, tudo o que precisamos é de um momento para pôr em palavras aquilo que realmente apreciamos. Para reencontrar o sentido do momento presente, aqui e agora.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "video",
        "alias": "Carta de agradecimento a quem valorizas",
        "title": "Carta de agradecimento a quem valorizas",
        "description": "“Amor, resiliência, compaixão, generosidade, eu aprendi isso com a minha mãe. A minha mãe é isso tudo.\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section4"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448861/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L6P3gratitudeautocompaixao_zdmnmc.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895814/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L6_Self_compassion_V1_rqvuff.mp4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "writing",
        "alias": "Carta de agradecimento a quem valorizas",
        "title": "Carta de agradecimento a quem valorizas",
        "time": "Tira 10-20 min",
        "shortDescription": "Escolhe uma pessoa da tua vida que valorizas, mas pela qual não tenhas tido a oportunidade de expressar a tua gratidão. Dedica um momento a refletir sobre o que essa pessoa fez para te sentires grata/o. Depois escreve-lhe uma carta, expressando a tua gratidão pelo que essa pessoa trouxe à tua vida. Escreve esta carta como se a fosses entregar pessoalmente, por exemplo, podes começar com \"Querida/o...\".",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "audio",
        "title": "Meditação",
        "description": "Agora presta atenção ao que se passa dentro de ti através desta curta meditação.",
        "audioUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621523404/29k%20FJN/Meditations/Joanna%20Final/Inner_Critic_yunh3r.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "editorialWithImage",
        "alias": "E se pudéssemos apreciar-nos da mesma forma que apreciamos os outros?",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448861/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L6P6gratitudeautocompaixao_eypwiv.jpg",
        "title": "E se pudéssemos apreciar-nos da mesma forma que apreciamos os outros?",
        "content": "A maioria das pessoas consegue ver os benefícios de expressar a gratidão aos outros, isso faz-nos crescer e fortalece a relação com os outros. No entanto, raramente expressamos gratidão para connosco, perdendo assim esses benefícios para nós próprios.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section7"
          }
        ]
      },
      {
        "id": "section7",
        "type": "writing",
        "alias": "Uma carta de agradecimento a ti própria/o",
        "title": "Uma carta de agradecimento a ti própria/o",
        "time": "Tira 10-20 min",
        "shortDescription": "Agora vamos mudar o foco da gratidão para dentro. Começa por escolher características tuas que aprecies mas pelas quais nunca tiveste oportunidade de expressar a tua gratidão. Agora reflete sobre as tuas conquistas e escolhas que fizeste na vida e pelas quais estás grata/o. Agora que já refletiste sobre estas áreas, escreve uma carta de gratidão para ti a partir desta perspetiva compassiva e agradecida.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "Parabéns!",
        "title": "Parabéns!",
        "content": "A tua ligação à gratidão pode fazer-te sentir poderosa/o, vulnerável e assustada/o. O próximo grande passo é ainda mais poderoso.\n\n### Deixa-os saber que agradeces\n\nSe for possível, o teu próximo desafio é expressar a tua gratidão à pessoa a quem escreveste a carta de agradecimento.\n\nRecomendamos que a encontres pessoalmente (ou como for possível) e a leias em voz alta.\n\nEsta é uma experiência muito forte.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section9"
          }
        ]
      },
      {
        "id": "section9",
        "type": "editorialWithImage",
        "alias": "# ~~Parabéns!~~",
        "title": "# ~~Parabéns!~~",
        "content": "Parabéns por completares a lição **Gratidão**, fizeste algo maravilhoso pelo teu bem-estar.\n\nSe decidires participar numa partilha com o teu grupo, vais precisar de ter à mão os exercícios:\n\n* Uma carta de agradecimento a alguém de quem gostas\n* Uma carta de agradecimento a ti mesma/o",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Fechar"
          }
        ]
      }
    ]
  }
}