{
  "en": {
    "id": "aee1f1ac-f795-4170-9ee5-1ee4bccfb961",
    "alias": "Coping with change 3: Act for change 1",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "61d21697-b3f9-4b48-ae33-48b974084466",
        "alias": "Act for change",
        "title": "Act for change",
        "content": "So far we've focused on exploring how change **affects** us and practicing **acceptance** in areas we can't change.\n\nIn this final lesson we'll look at the areas in your life you **can** **control**. Acting for change is a final ingredient that helps you continuously **move** your **life** in a **meaningful direction**.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "I'm ready",
            "sectionId": "2aed4035-e1d2-422a-ad76-65448391f16c"
          }
        ],
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1629709101/cagatay-orhan-PYh4QCX_fmE-unsplash_f6ipy3.jpg"
      },
      {
        "type": "editorialWithImage",
        "id": "2aed4035-e1d2-422a-ad76-65448391f16c",
        "content": "### Learn more about:\n\n* Taking action\n* Problem-solving\n* Practical steps\n\n### You’ll need:\n\n* 20 minutes\n* Pen and paper",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Let's go",
            "sectionId": "8b15f378-5903-4e1c-aa12-dfd3c5ecad24"
          }
        ],
        "alias": "Today’s lesson"
      },
      {
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "I'm done",
            "sectionId": "3747be04-af14-4f99-9b58-a25157b76c53"
          }
        ],
        "alias": "Meditation",
        "imageUri": "",
        "duration": "6:45",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/meditations/the-breathing-anchor.mp3",
        "title": "Meditation",
        "type": "audio",
        "id": "8b15f378-5903-4e1c-aa12-dfd3c5ecad24",
        "description": "Before we get going, let's take a moment to arrive. This meditation will help you feel more **mindful** and **present**, and prepare you for the exercise you're about to do.\n\nPress **play** to start."
      },
      {
        "type": "editorialWithImage",
        "id": "3747be04-af14-4f99-9b58-a25157b76c53",
        "title": "",
        "alias": "anxiety cycle",
        "content": "Previously you've identified what's out of your **control** and what you're willing to **accept**.\n\nDwelling and worrying may **seem** like **problem-solving**, but in fact we make no real progress solving things as long as we **remain stuck** in an **anxiety** **cycle**.\n\nBut in almost every situation — no matter how challenging — there's meaningful **action** we can take.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Got it",
            "sectionId": "e1aee658-ade0-48c0-bc3f-77ee9e92f334"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "e1aee658-ade0-48c0-bc3f-77ee9e92f334",
        "content": "Now it's time for you to explore one thing that is currently contributing to your anxiety, and **identify** what you can do to **change** it — from brainstorming **solutions** to taking concrete **action**.\n\nSuch changes don’t have to be drastic. Sometimes **tweaking** our **actions** is enough to make a significant **difference** to our well-being.",
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Next",
            "conversationId": "82790697-a182-46fa-8867-c638ed62a587"
          }
        ],
        "alias": "Take action"
      }
    ],
    "root": "61d21697-b3f9-4b48-ae33-48b974084466"
  },
  "sv": {
    "id": "aee1f1ac-f795-4170-9ee5-1ee4bccfb961",
    "alias": "Coping with change 3: Act for change 1",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "da9f59a5-64c9-464e-91cb-3b7a2d404e80",
        "alias": "Act for change",
        "title": "Förändra det som inte fungerar",
        "content": "När vi är mitt uppe i en kris, är det lätt hänt att ångesten tar över.\n\nI den här lektionen får du lära dig att ta **praktiska** **steg** mot det som är viktigt i ditt liv även i svåra situationer.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Jag är redo",
            "sectionId": "a791f696-9456-46ba-9afb-8a4f44a5feef"
          }
        ],
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1629709101/cagatay-orhan-PYh4QCX_fmE-unsplash_f6ipy3.jpg"
      },
      {
        "type": "editorialWithImage",
        "id": "a791f696-9456-46ba-9afb-8a4f44a5feef",
        "content": "### Lär dig mer om:\n\n* Landa i nuet\n* Problemlösning\n* Ta praktiska steg\n\n### Du behöver:\n\n* 20 minuter \n* Penna och papper",
        "alias": "Dagens lektion",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Sätt igång",
            "sectionId": "9092aedd-0734-4a8e-b219-30426212c035"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "9092aedd-0734-4a8e-b219-30426212c035",
        "alias": "Att ta praktiska steg",
        "title": "",
        "content": "I livet möter vi alla stressande och smärtsamma situationer. Ibland känns det som att vi **inte har något annat val** än att **acceptera** dem.\n\nDå kan **ångesten** kännas som ett mörkt hål av osäkerhet som får oss att känna oss **förlamade**. Det gäller särskilt när vi befinner oss mitt i en kris.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Fortsätt",
            "sectionId": "b1ae3d55-0caf-4dd3-acc2-0f6e03feab93"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "b1ae3d55-0caf-4dd3-acc2-0f6e03feab93",
        "content": "Men i nästan alla situationer – oavsett hur utmanande – kan vi **göra** något meningsfullt. \n\nAtt älta och oroa sig kan **kännas** som ett slags **problemlösning** för hjärnan, men i själva verket kommer vi oftast ingenstans, utan fastnar i en **ångestspiral**.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Berätta mer",
            "sectionId": "2534e0f5-9e64-4c42-9607-f0f13ec321cd"
          }
        ],
        "alias": "Agera"
      },
      {
        "type": "editorialWithImage",
        "id": "2534e0f5-9e64-4c42-9607-f0f13ec321cd",
        "content": "Nu är det dags för dig att ta tag i det mörka hålet och ta till konkreta åtgärder. \n\nDet behöver inte vara någon jättestor sak. Ibland räcker det med att **ändra** ett **litet** beteende för att få till en stor **förändring**.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Nästa",
            "sectionId": "4ccd7efa-1a8e-46b8-9347-baf3bb27229f"
          }
        ],
        "alias": "Tweak your actions"
      },
      {
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Jag är klar",
            "conversationId": "82790697-a182-46fa-8867-c638ed62a587"
          }
        ],
        "alias": "Meditation",
        "imageUri": "",
        "duration": "6:45",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/meditations/the-breathing-anchor.mp3",
        "title": "Landa i nuet",
        "type": "audio",
        "id": "4ccd7efa-1a8e-46b8-9347-baf3bb27229f",
        "description": "Den här meditation hjälper dig bli dig mer **uppmärksam** och **närvarande**, samtidigt som den förbereder dig för nästa övning.\n\nTryck på play-knappen för att börja."
      }
    ],
    "root": "da9f59a5-64c9-464e-91cb-3b7a2d404e80"
  },
  "pt": {
    "id": "aee1f1ac-f795-4170-9ee5-1ee4bccfb961",
    "alias": "Coping with change 3: Act for change 1",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "82c87910-2732-45a1-8c74-7c60e68063c6",
        "alias": "Agir para mudar",
        "title": "Agir para mudar",
        "content": "Dar passos que **movem a tua vida numa direção com significado** é muito importante quando estás numa crise e a lidar com ansiedade.\n\nEsta lição ensina a **adotar medidas práticas** que podes utilizar em situações desafiantes.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Continuar",
            "sectionId": "19fcc3da-a5a0-4880-a2c7-430f34c008dc"
          }
        ],
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1629709101/cagatay-orhan-PYh4QCX_fmE-unsplash_f6ipy3.jpg"
      },
      {
        "type": "editorialWithImage",
        "id": "19fcc3da-a5a0-4880-a2c7-430f34c008dc",
        "content": "### Vais aprender mais acerca de:\n\n* Meditação\n* Resolução de problemas\n* Adotar medidas práticas\n\n### O que vais precisar:\n\n* 20 min de sossego sem interrupções\n* Caneta e papel",
        "alias": "Today's lesson",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Continuar",
            "sectionId": "656a0376-e640-472a-aebd-f8eae6825caf"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "656a0376-e640-472a-aebd-f8eae6825caf",
        "alias": "Adotar medidas práticas",
        "title": "Adotar medidas práticas",
        "content": "Na nossa vida encontramos muitas situações difíceis, stressantes, dolorosas e sentimos frequentemente que **não temos outra escolha** para além de **aceitá-las**.\n\nIsto é particularmente verdade no meio de uma crise, quando a **ansiedade** parece uma nuvem negra de incerteza que nos deixa **paralisados**.",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Continuar",
            "sectionId": "232ed37b-4bbb-4c6e-8cab-725dcc0973c0"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "232ed37b-4bbb-4c6e-8cab-725dcc0973c0",
        "alias": "Agir",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Continuar",
            "sectionId": "d77a8001-ba13-46de-9eac-aba6644c7b19"
          }
        ],
        "content": "Mas na maioria das situações, não importa o quão desafiantes são, podes escolher **agir** e fazer alguma coisa.\n\nAo cismares e preocupares-te pode **parecer** que estás a **resolver o problema**, mas, na realidade, não progrides enquanto estiveres **preso** no **ciclo da ansiedade**."
      },
      {
        "type": "editorialWithImage",
        "id": "d77a8001-ba13-46de-9eac-aba6644c7b19",
        "alias": "Ajusta as tuas ações",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Próximo",
            "sectionId": "d85c0bc2-83ca-4a1e-a86d-5d9900e7c82d"
          }
        ],
        "content": "Esta lição ensina-te a entrar em contacto com essa nuvem negra e a adotar ações concretas.\n\nEstas mudanças não têm de ser dramáticas. Muitas vezes, **pequenos ajustes** nas tuas **ações** são suficientes para um **impacto positivo** no teu bem-estar."
      },
      {
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Estou pronta/o",
            "conversationId": "82790697-a182-46fa-8867-c638ed62a587"
          }
        ],
        "alias": "Meditação",
        "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1620635336/Illustrations_Meditations/Thumbnail_Breathing_anchor_1_j1kyar.png",
        "duration": "6:01",
        "audioUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621429299/29k%20FJN/Meditations/Pedro%20Final/Medita%C3%A7%C3%B5es_10_-_Breathing_Anchor_r7zkxc.mp3",
        "title": "Meditação",
        "type": "audio",
        "id": "d85c0bc2-83ca-4a1e-a86d-5d9900e7c82d",
        "description": "Esta meditação vai ajudar-te a sentires-te mais **consciente** e **presente**, sendo uma preparação para o próximo exercício."
      }
    ],
    "root": "82c87910-2732-45a1-8c74-7c60e68063c6"
  }
}