{
  "en": {
    "id": "K12W56Qapx57nppRtyzioE-4",
    "alias": "Self compassion - Self-care tool 4",
    "type": "lesson",
    "root": "section8",
    "sections": [
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "What tools are you in need of instead?",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_writing_2.jpg",
        "title": "What tools are you in need of instead?",
        "content": "We are all put in situations when it’s difficult to be self-caring.\n\nAt those times it’s a good thing to have a plan. An emergency toolbox. A set of suggestions of how to handle the challenging situation in a way that lets you be self-compassionate.",
        "buttons": [
          {
            "component": "Button",
            "content": "Create my own self-care tool",
            "sectionId": "section9"
          }
        ],
        "infoItems": [
          {
            "title": "About the process",
            "description": "We will guide you through a 3-step simple process created by Drs. Kristin Neff and Christopher Germer in the Mindful Self-Compassion (MSC) program."
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "1. Notice a moment of suffering. How can you notice and acknowledge that you are in a moment of suffering? For example:\n\n> “There’s that feeling in my chest again.”\n\n- - -\n\n> “Ah, something is happening inside of me. This is a painful moment for me.”",
        "buttons": [
          {
            "component": "Button",
            "content": "I'm done",
            "sectionId": "section10"
          }
        ],
        "question": "",
        "time": "10 minutes",
        "alias": "My emergency toolbox",
        "title": "My emergency toolbox",
        "type": "writing",
        "id": "section9"
      },
      {
        "shortDescription": "",
        "longDescription": "2. How can you remind yourself that suffering is a part of life and that you’re not alone? For example:\n\n> “What I am feeling right now is a part of being human, even though it feels like I am alone with this painful experience.”\n\n---\n\n> “Pain is part of life. Everyone experiences pain. I’m not alone.”",
        "buttons": [
          {
            "component": "Button",
            "content": "I'm done",
            "sectionId": "section11"
          }
        ],
        "question": "Common humanity",
        "time": "",
        "alias": "Common humanity",
        "title": "",
        "type": "writing",
        "id": "section10"
      },
      {
        "shortDescription": "",
        "longDescription": "3. Lastly, how can you be kind to yourself? For example:\n\n> “Is there something I can do to care for myself in this moment?”\n\n---\n\n> “You are trying your best, that’s enough for me.”",
        "buttons": [
          {
            "component": "Button",
            "content": "I'm done",
            "sectionId": "section14"
          }
        ],
        "question": "",
        "time": "",
        "alias": "Acting kindly",
        "title": "Acting kindly",
        "type": "writing",
        "id": "section11"
      },
      {
        "id": "section14",
        "type": "video",
        "alias": "Rachel’s reflection",
        "title": "Rachel’s reflection",
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Continue",
            "conversationId": "K12W56Qapx57nppRtyzioE-5"
          }
        ],
        "videos": [
          {
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622462871/Self%20Compassion/selfcaretool_exercise2_lsfzmd.mp4",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1635236820/Self%20Compassion/Yoga_Girl_005710_rdyzhf.jpg"
          }
        ]
      }
    ]
  },
  "sv": {
    "id": "K12W56Qapx57nppRtyzioE-4",
    "alias": "Self compassion - Self-care tool 4",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "video",
        "alias": "En situation där jag tappar fot ...",
        "title": "En situation där jag tappar fot ...",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section2"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/video-images/yogagirl_selfcaretool_intro.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622207009/Self%20Compassion/selfcaretool_intro_daymos.mp4"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Självvårdsverktyget",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_intro_1-min.jpg",
        "title": "Självvårdsverktyget",
        "content": "### Trevligt att se dig tillbaka.\n\nI den här lektionen kommer du att undersöka hur du kan bli bättre på att märka när du behöver självmedkänsla och hur du kan öva självvård under dessa svåra tider.\n\n### Forskningen\n\nDen här lektionen är inspirerad av den bevisbaserade övningen ”Self-Compassion Break” i MSC.\n\n### Dagens lektion består av tre delar:\n\n* Hitta situationer när jag behöver självmedkänsla\n* Meditation om egenvård\n* Skapa en nödverktygslåda\n\n### Förberedelse för lektionen:\n\n* 30 min ostörd tid\n* Penna och papper\n",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "Självvårdsverktyget",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_intro_2.jpg",
        "title": "Självvårdsverktyget",
        "content": "### Det är när vi kämpar mest, vi är ofta svårast på oss själva.\n\nDet är lätt att tro att det att vi är trevliga mot oss själva gör oss svaga. Mjuk. Omotiverad.\n\nSå vi driver. Hårdare.\n\nMen forskning visar att motsatsen är sant. Medkänsla gör oss starkare, mer motiverade och motståndskraftiga.\n\nFör att träna denna förmåga måste du först börja märka de små vardagssituationerna när du tappar fot. Låt oss börja med att lyssna på ett exempel på när Rachel fångade sig själv.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "video",
        "alias": "Blir överväldigad",
        "title": "Blir överväldigad",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section5"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/video-images/yogagirl_selfcaretool_exercise1.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622207139/Self%20Compassion/selfcaretool_exercise1_hsn2c3.mp4"
          }
        ]
      },
      {
        "id": "section5",
        "type": "editorialWithImage",
        "alias": "Introduktion",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_writing_1.jpg",
        "title": "Introduktion",
        "content": "Reflektera över den senaste veckan, se om du kan välja ett par situationer när du förlorade ditt självmedkännande jag.\n\nFörsök att föreställa dig situationerna och gå in i dem efter varandra.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section6"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "### Fyll i meningarna nedan.\n\n* En situation då jag tappade fot var när ...\n* Jag känner mig vanligtvis otillräcklig när ...\n* När jag behöver egenvård mest är när ...",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section7"
          }
        ],
        "question": "",
        "time": "Ta fem minuter",
        "alias": "Förlora vår fot",
        "title": "Förlora vår fot",
        "type": "writing",
        "id": "section6"
      },
      {
        "id": "section7",
        "type": "audio",
        "title": "Meditation",
        "description": "Du är inbjuden till en kort meditation för att göra dig redo för nästa del av denna lektion.",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/yg_self_compassion/self_care_tool/yogagirl_meditation_Lesson_04_SOS_meditation.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "Introduktion",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_writing_2.jpg",
        "title": "Introduktion",
        "content": "#### Vilka verktyg behöver du?\n\nVi sätts alla i situationer när det är svårt att vara självvårdande.\n\nDå är det bra att ha en plan. En nödverktygslåda. En uppsättning förslag på hur man hanterar den utmanande situationen på ett sätt som låter dig vara självmedkännande.\n\nSå låt oss skapa ditt eget verktyg. Vi guidar dig genom tre enkla och jordnära steg skapade av Drs. Kristin Neff och Christopher Germer i programmet Mindful Self-Compassion (MSC). Vi hjälper dig att skapa egna verktyg för:\n\nVad du kan göra för att ...\n\n1. märk ett lidande ögonblick\n2. se lidande som en del av att vara människa\n3. agera vänligt mot dig själv",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section9"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "#### Skapa din egen nödverktygslåda\n\n#### 1. Lägg märke till ett lidande ögonblick\n\nFör det första, skriv ner i dina egna ord:\n\nHur kan du märka och erkänna att du befinner dig i ett ögonblick av lidande?\n\nT.ex.\n\n> \"Det finns den känslan i mitt bröst igen.\"\n\n---\n\n> “Ah, det händer något inuti mig. Det här är ett smärtsamt ögonblick för mig. ”",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section10"
          }
        ],
        "question": "",
        "time": "Ta tre minuter",
        "alias": "Skapa ditt eget vårdverktyg",
        "title": "Skapa ditt eget vårdverktyg",
        "type": "writing",
        "id": "section9"
      },
      {
        "shortDescription": "",
        "longDescription": "#### Skapa din egen nödverktygslåda\n\n#### 2. Gemensam mänsklighet\n\nSkriv nu ned i dina egna ord:\n\n#### Hur kan du påminna dig själv om att lidande är en del av livet och att du inte är ensam?\n\nT.ex.\n\n> \"Det jag känner just nu är en del av att vara människa, även om det känns som att jag är ensam med denna smärtsamma upplevelse.\"\n\n---\n\n> ”Smärta är en del av livet. Alla. Jag är inte ensam om detta. ”",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section11"
          }
        ],
        "question": "",
        "time": "Ta tre minuter",
        "alias": "Skapa ditt eget vårdverktyg",
        "title": "Skapa ditt eget vårdverktyg",
        "type": "writing",
        "id": "section10"
      },
      {
        "shortDescription": "",
        "longDescription": "#### Skapa din egen nödverktygslåda\n\n#### 3. Handlar vänligt\n\nSlutligen, skriv i dina egna ord:\n\n#### Hur kan du vara snäll mot dig själv?\n\nT.ex.\n\n> \"Finns det något jag kan göra för att ta hand om mig själv just nu?\"\n\n---\n\n> \"Du gör ditt bästa, det räcker för mig.\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section12"
          }
        ],
        "question": "",
        "time": "Ta tre minuter",
        "alias": "Skapa ditt eget vårdverktyg",
        "title": "Skapa ditt eget vårdverktyg",
        "type": "writing",
        "id": "section11"
      },
      {
        "id": "section12",
        "type": "video",
        "alias": "Rachels egenvårdsverktyg",
        "title": "Rachels egenvårdsverktyg",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section13"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/video-images/yogagirl_selfcaretool_exercise2.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622462871/Self%20Compassion/selfcaretool_exercise2_lsfzmd.mp4"
          }
        ]
      },
      {
        "id": "section13",
        "type": "editorialWithImage",
        "alias": "Självvårdsverktyget",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/yg_self_care_tool_writing_3.jpg",
        "title": "Självvårdsverktyget",
        "content": "#### Wow!\n\nDu har lagt ner mycket arbete på att skapa din egen verktygslåda för nödsituationer. Förhoppningsvis kommer du att ha stor nytta av detta i tider när du upplever svåra och smärtsamma känslor.\n\n#### Använd din verktygslåda när du mest behöver den\n\nNästa gång du upplever en svår eller utmanande situation som du beskrev under den här lektionen, prova din nödverktygslåda.\n\nGör gärna ändringar i din nödverktygslåda baserat på dina erfarenheter av vad som fungerar bra för dig.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section14"
          }
        ]
      },
      {
        "id": "section14",
        "type": "video",
        "alias": "Rachels reflektion",
        "title": "Rachels reflektion",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section15"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/self_care_tool/video-images/yogagirl_selfcaretool_outro.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622462930/Self%20Compassion/selfcaretool_outro_wqvu93.mp4"
          }
        ]
      },
      {
        "id": "section15",
        "type": "editorialWithImage",
        "alias": "# Bra ~~jobbat!~~",
        "title": "# Bra ~~jobbat!~~",
        "content": "Bra gjort när du har slutfört lektionen ** Självvård **, du har precis gjort en underbar sak för dig själv.\n\nOm du väljer att delta i en delning med din grupp behöver du följande övningar nära till hands:\n\n* Förlora vår fot\n* Skapa ditt eget vårdverktyg\n",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Stäng"
          }
        ]
      }
    ]
  },
  "pt": {
    "id": "K12W56Qapx57nppRtyzioE-4",
    "alias": "Self compassion - Self-care tool 4",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "video",
        "alias": "Uma situação em que fico sem chão...",
        "title": "Uma situação em que fico sem chão...",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section2"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P1selfcareautocompaixao_mf6gg3.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622374570/29k%20FJN/Courses/Self%20Compassion/Videos%2020210530%20Update/FL_-_L5_Self_compassion_V1_anlygi.mp4"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "As ferramentas de autocuidado",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P2selfcareautocompaixao_h16jqa.jpg",
        "title": "As ferramentas de autocuidado",
        "content": "### É bom ter-te de volta.\n\nNesta sessão vais descobrir como podes melhorar a aperceberes-te de quando precisas de autocompaixão e como podes praticar o autocuidado durante tempos difíceis.\n\n### A investigação\n\nEsta sessão é inspirada no exercício do MSC \"A Pausa para a Autocompaixão\".\n\n### A sessão de hoje tem três partes:\n\n* Encontrar situações em que precisas de autocompaixão\n* Meditação para o autocuidado\n* Criar uma caixa de ferramentas para emergências\n\n### Preparação para esta sessão:\n\n* 30 min de tempo sem perturbações\n* Papel e caneta",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "As ferramentas de autocuidado",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P3selfcareautocompaixao_uzvukp.jpg",
        "title": "As ferramentas de autocuidado",
        "content": "### Muitas vezes, é nos momentos mais difíceis e desafiantes que somos mais duros para connosco.\n\nÉ fácil acreditarmos que sermos amáveis para connosco nos torna fracos, moles e desmotivados.\n\nPor isso, exigimos mais de nós. Cada vez mais.\n\nMas a investigação demonstra-nos precisamente o oposto. A autocompaixão torna-nos fortes, mais motivados e resilientes.\n\nPara desenvolver esta habilidade, primeiro tens de começar por notar quais são as pequenas situações diárias que te abalam. Vamos começar por ouvir um exemplo de quando a Fátima se apercebeu dessas situações.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "video",
        "alias": "Ficar assoberbada/o",
        "title": "Ficar assoberbada/o",
        "description": "“É normal sentires o que estás a sentir. Só sentes isso porque tu não controlas e porque é imprevisível.”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section5"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P4selcareyautocompaixao_jich0o.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895849/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L5_Self_compassion_V2_llnol2.mp4"
          }
        ]
      },
      {
        "id": "section5",
        "type": "editorialWithImage",
        "alias": "Introdução",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448860/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P5selfcareautocompaixao_jc5y6w.jpg",
        "title": "Introdução",
        "content": "Reflete sobre a tua última semana, procura escolher algumas situações em que perdeste a tua autocompaixão.\n\nTenta visualizar as situações e volta a elas, uma após a outra.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "### Completa as frases abaixo.\n\n* Uma situação em que senti que perdi o chão foi quando...\n* Normalmente sinto-me inadequada/o quando...\n* Quando mais preciso de autocuidado é quando...",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section7"
          }
        ],
        "question": "",
        "time": "Tira 5 min",
        "alias": "Perder o chão",
        "title": "Perder o chão",
        "type": "writing",
        "id": "section6"
      },
      {
        "id": "section7",
        "type": "audio",
        "title": "Meditação",
        "description": "Convidamos-te para uma breve meditação que te vai preparar para a próxima parte da sessão.",
        "audioUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621523418/29k%20FJN/Meditations/Joanna%20Final/The_Way_Out_is_Throught_tgmfsb.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "Introdução",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448860/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P8selfcareautocompaixao_fecgxg.jpg",
        "title": "Introdução",
        "content": "#### De que ferramentas estás a precisar?\n\nTodos nós somos colocados em situações em que nos é difícil cuidar de nós próprios.\n\nNessas alturas, é bom ter um plano. Uma caixa de ferramentas para emergências. Um conjunto de sugestões para lidar com uma situação desafiante, de uma forma que te permita ser autocompassiva/o.\n\nVamos então criar as tuas ferramentas. Vamos guiar-te através de três passos simples e pragmáticos, criados pelos Drs. Kristin Neff e Christopher Germer para o programa *Mindful Self-Compassion* (MSC). Vamos ajudar-te a criar as tuas próprias ferramentas.\n\nO que podes fazer para...\n\n1. Notar um momento de sofrimento\n2. Ver o sofrimento como uma parte de ser humano\n3. Agir com bondade para contigo",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section9"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "#### Constrói a tua caixa de ferramentas para emergências\n\n#### 1. Apercebe-te de um momento de sofrimento\n\nPrimeiro, escreve por palavras tuas:\n\nComo podes dar-te conta e reconhecer que estás num momento de sofrimento?\n\nExemplo:\n\n> “Sinto outra vez aquela sensação no meu peito.”\n\n- - -\n\n> “Ah, algo está a acontecer dentro de mim. Este é um momento doloroso para mim.”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section10"
          }
        ],
        "question": "",
        "time": "Tira 3 min",
        "alias": "Cria as tuas ferramentas de autocuidado",
        "title": "Cria as tuas ferramentas de autocuidado",
        "type": "writing",
        "id": "section9"
      },
      {
        "shortDescription": "",
        "longDescription": "#### Constrói a tua caixa de ferramentas para emergências\n\n#### 2. Humanidade Comum\n\nAgora, escreve por palavras tuas:\n\n#### Como podes relembrar-te de que o sofrimento faz parte da vida e de que não estás sozinha/o?\n\nExemplo:\n\n> “O que estou a sentir agora faz parte de ser humano, ainda que pareça que estou sozinha/o nesta experiência dolorosa.\"\n\n- - -\n\n> “A dor faz parte da vida de todos os seres humanos. Não estou sozinha/o nisto.\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section11"
          }
        ],
        "question": "",
        "time": "Tira 3 min",
        "alias": "Cria as tuas ferramentas de autocuidado",
        "title": "Cria as tuas ferramentas de autocuidado",
        "type": "writing",
        "id": "section10"
      },
      {
        "shortDescription": "",
        "longDescription": "#### Constrói a tua caixa de ferramentas para emergências\n\n#### 3. Agir com bondade\n\nPor último, escreve por palavras tuas:\n\n#### Como podes ser amável para contigo?\n\nExemplo\n\n> “Posso fazer alguma coisa para cuidar de mim neste momento?\"\n\n- - -\n\n> “Estou a dar o meu melhor, isso é suficiente para mim.\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section12"
          }
        ],
        "question": "",
        "time": "Tira 3 min",
        "alias": "Cria as tuas ferramentas de autocuidado",
        "title": "Cria as tuas ferramentas de autocuidado",
        "type": "writing",
        "id": "section11"
      },
      {
        "id": "section12",
        "type": "video",
        "alias": "As ferramentas de autocuidado da Fátima",
        "title": "As ferramentas de autocuidado da Fátima",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section13"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448860/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5P12bodyautocompaixao_ytn7cd.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622374512/29k%20FJN/Courses/Self%20Compassion/Videos%2020210530%20Update/FL_-_L5_Self_compassion_V3_mn2oy9.mp4"
          }
        ]
      },
      {
        "id": "section13",
        "type": "editorialWithImage",
        "alias": "As ferramentas de autocuidado",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448860/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L5p13bodyautocompaixao_lv6c80.jpg",
        "title": "As ferramentas de autocuidado",
        "content": "#### Uau!\n\nFizeste um grande esforço para criar a tua própria caixa de ferramentas para emergências. Esperemos que venhas a beneficiar bastante dela, em momentos em que experiencies sentimentos difíceis e dolorosos.\n\n#### Usa a tua caixa de ferramentas quando mais precisares\n\nDa próxima vez que experienciares uma situação difícil ou desafiante, como as que descreveste durante esta sessão, experimenta a tua caixa de ferramentas para emergências.\n\nSente-te à vontade para lhe fazeres as alterações que achares necessárias, de acordo com as tuas experiências e do que sintas que está a resultar para ti.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section14"
          }
        ]
      },
      {
        "id": "section14",
        "type": "editorialWithImage",
        "alias": "# ~~Parabéns!~~",
        "title": "# ~~Parabéns!~~",
        "content": "Parabéns por completares a sessão **Ferramentas de** **Autocuidado**, fizeste uma coisa maravilhosa pelo teu bem-estar.\n\nSe decidires partilhar com o teu grupo, vais precisar de ter à mão os seguintes exercícios:\n\n* Ficar sem chão\n* Criar a tuas ferramentas de autocuidado",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Fechar"
          }
        ]
      }
    ]
  }
}