{
  "en": {
    "hideOnHome": false,
    "segments": [
      "ADULTS"
    ],
    "author": {
      "nickname": ""
    },
    "name": "Deepening relationships",
    "about": {
      "howThisWorks": "#### How does it work?\n* This course takes 4 weeks to complete\n* One lesson per week\n* One video sharing per week",
      "courseValue": "#### What will I learn?\n\n* Find courage to love\n* Create safety for others to do the same\n* Learn how to fully express yourself in a relationship",
      "body": "What would your life look like if you were able to fully express yourself in relationships, with courage and with love, to create safety for others to do the same, and to accept and let in the love that other people have to offer you?\n\nThis course explores how to grow relationships and take bold steps in letting your true self come to life in relationships. Whether you want to strengthen a relationship to a friend, partner, colleague or family member, this course can help you get going. The basic formula backed up by science is: spending time x being courageous x being loving x being aware = deeper connection.",
      "createdBy": {
        "text": "This course has been created in collaboration with the renown relationship researchers Mavis Tsai and Jonathan W Kanter, and was edited by 29k psychologist Daniel Ek. The content in the course is based on well-researched exercises and interventions and a list of scientific references can be found on our website [29k.org/research](www.29k.org/research)",
        "profiles": [
          {
            "name": "Jonathan W Kanter, PhD",
            "linkUri": "https://depts.washington.edu/uwcssc/content/jonathan-w-kanter-phd",
            "imageUri": "https://392xdjcckkb3mwu5z3dq8ym1-wpengine.netdna-ssl.com/wp-content/uploads/2020/07/jonathan-featured.png"
          },
          {
            "name": "Mavis Tsai, PhD",
            "linkUri": "https://depts.washington.edu/uwcssc/content/mavis-tsai-phd",
            "imageUri": "https://www.lowcountrymhconference.com/wp-content/uploads/2020/01/Mavis-Tsai-2-1-e1579792151943.jpg"
          },
          {
            "name": "Daniel Ek, lic psychologist",
            "linkUri": "https://www.vanskapslabbet.se",
            "imageUri": "https://www.hjarnkraft.com/wp-content/uploads/bfi_thumb/1-lejrclfzc53atuc90fc2nkhg8kcytldllp8bddxi7o.jpg"
          }
        ]
      }
    },
    "featured": false,
    "background": {
      "uri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg",
      "color": "#223D37"
    },
    "cta": "Explore and develop skills for creating deeper connections with other human beings.",
    "ratingCount": 460,
    "duration": {
      "count": 4,
      "type": "week"
    },
    "coverUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg",
    "tags": [
      "Relationships"
    ],
    "id": "9a5b146a-61b9-4aef-8519-90304320dcf1",
    "lessons": [
      {
        "id": "bcecd21c-811b-46ac-a0d1-5e549cdfc8b1",
        "name": "Letting love in",
        "index": 0,
        "time": "40 min",
        "description": "Learn how to accept love from others and create moments of connection.",
        "completionScreen": {
          "content": "### Practice for the coming week:\n\nLearning how to let love in is a lifelong process for most of us. This week, your mission is to notice opportunities, even small ones, to let love in. Notice how you feel in these moments, and practice leaning in and opening up to the feelings and even savour them. Practice also expressing appreciation to the person who was caring.\n\nKeep a diary, and complete it at the end of each day:  Write down:\n\n* Any opportunities you observed today?\n* What did you feel in your body?\n* On a scale of 0 (not at all) to 7 (completely), how much were you able to let love in?\n\n* Did you express acceptance to the person who created the opportunity?\n"
        },
        "conversations": [
          {
            "conversationId": "EFSZaU3Tk4dCV5GXPG27bx",
            "title": "Letting love in"
          }
        ]
      },
      {
        "id": "9321387f-2b32-4431-aa06-9d663a267b55",
        "name": "Nourishing relationships",
        "index": 1,
        "time": "40 min",
        "description": "Explore your relationships and take action to come closer.",
        "completionScreen": {
          "content": "Well done on completing the relationship map. This week zoom in on especially meaningful and nourishing moments as they are happening. Do this by slowing down, really noticing what you and the other person are doing and becoming aware of what feelings and sensations you experience in your body.\n"
        },
        "conversations": [
          {
            "conversationId": "S5PVao18y6KQHEBq92u9w4",
            "title": "Nourishing relationships"
          }
        ]
      },
      {
        "id": "05eb4188-e12d-42e5-aaca-ffe258b77780",
        "name": "Showing yourself",
        "index": 2,
        "time": "40 min",
        "description": "Create deeper connections by being vulnerable.",
        "completionScreen": {
          "content": "Being vulnerable can feel really scary. This week, see if you can notice moments where you can show yourself, what you feel, need or think and try to be just a bit more open.\n"
        },
        "conversations": [
          {
            "conversationId": "JEQeL6RPUbmDkdEaaJ94bc",
            "title": "Showing yourself"
          }
        ]
      },
      {
        "id": "2f4980c2-62c2-4685-b7e5-3cd0dea02151",
        "name": "Responding well",
        "index": 2,
        "time": "40 min",
        "description": "Learn how to recognize and respond when others take steps to deepen a relationship with you",
        "completionScreen": {
          "content": "Learning how to respond well is a lifelong process for most of us.  It is not easy.  Start by being a vulnerability hawk. Notice small moments of vulnerability throughout your day. Tell yourself - these moments are real, and important. See if you can respond with safety, validation and care."
        },
        "conversations": [
          {
            "conversationId": "H38X7X4tuwRdm7rGPLe2dj",
            "title": "Responding well"
          }
        ]
      }
    ],
    "rating": 4.4,
    "backgroundImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg"
  },
  "sv": {
    "hideOnHome": false,
    "segments": [
      "ADULTS"
    ],
    "author": {
      "nickname": ""
    },
    "name": "Fördjupade relationer",
    "about": {
      "howThisWorks": "#### Hur fungerar det?\n* Kursen tar fyra veckor att genomföra\n* En lektion per vecka\n* En gruppdelning per vecka",
      "courseValue": "#### Vad ska jag lära mig?\n* Hitta mod att älska\n* Skapa säkerhet för andra att göra detsamma\n* Lär dig hur du kan uttrycka dig fullständigt i ett förhållande\n",
      "body": "Hur skulle ditt liv se ut om du kunde uttrycka dig fullständigt i relationer, med mod och med kärlek, skapa säkerhet för andra att göra detsamma och acceptera och släppa in den kärlek som andra människor har att erbjuda dig?\n\nDen här kursen utforskar hur man växer relationer och tar djärva steg för att låta ditt sanna jag leva upp i relationer. Oavsett om du vill stärka ett förhållande till en vän, partner, kollega eller familjemedlem, kan den här kursen hjälpa dig att komma igång. Grundformeln som backas upp av vetenskapen är: spendera tid x vara modig x vara kärleksfull x vara medveten = djupare koppling."
    },
    "background": {
      "uri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg",
      "color": "#223D37"
    },
    "cta": "Utforska och utveckla färdigheter för att skapa djupare kontakter med andra.",
    "ratingCount": 460,
    "duration": {
      "count": 4,
      "type": "week"
    },
    "coverUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg",
    "tags": [
      "Relationer"
    ],
    "id": "9a5b146a-61b9-4aef-8519-90304320dcf1",
    "lessons": [
      {
        "id": "bcecd21c-811b-46ac-a0d1-5e549cdfc8b1",
        "name": "Släppa in kärleken",
        "index": 0,
        "time": "40 min",
        "description": "Lär dig hur du accepterar kärlek från andra och skapar stunder av anslutning.",
        "completionScreen": {
          "content": "### Övning för den kommande veckan:\n\nAtt lära sig att släppa in kärlek är en livslång process för de flesta av oss. Den här veckan är ditt uppdrag att lägga märke till möjligheter, även små, att släppa in kärleken. Lägg märke till hur du känner dig i dessa ögonblick och öva att luta dig in och öppna upp för känslorna och till och med njuta av dem. Öva också på att uttrycka uppskattning till den som brydde sig.\n\nHåll en dagbok och slutför den i slutet av varje dag: Skriv ner:\n\n* Några möjligheter du har observerat idag?\n* Vad kände du i din kropp?\n* På en skala från 0 (inte alls) till 7 (helt), hur mycket kunde du släppa in kärleken?\n\n* Uttryckte du acceptans för den person som skapade möjligheten?\n"
        },
        "conversations": [
          {
            "conversationId": "EFSZaU3Tk4dCV5GXPG27bx",
            "title": "Släppa in kärleken"
          }
        ]
      },
      {
        "id": "9321387f-2b32-4431-aa06-9d663a267b55",
        "name": "Närande relationer",
        "index": 1,
        "time": "40 min",
        "description": "Utforska dina relationer och vidta åtgärder för att komma närmare.",
        "completionScreen": {
          "content": "Bra gjort med att slutföra relationskartan. Den här veckan zoomar in på särskilt meningsfulla och närande stunder när de händer. Gör detta genom att sakta ner, verkligen märka vad du och den andra gör och bli medveten om vilka känslor och känslor du upplever i din kropp.\n"
        },
        "conversations": [
          {
            "conversationId": "S5PVao18y6KQHEBq92u9w4",
            "title": "Närande relationer"
          }
        ]
      },
      {
        "id": "05eb4188-e12d-42e5-aaca-ffe258b77780",
        "name": "Visar dig själv",
        "index": 2,
        "time": "40 min",
        "description": "Skapa djupare kopplingar genom att vara sårbar.",
        "completionScreen": {
          "content": "Att vara sårbar kan kännas riktigt skrämmande. Den här veckan, se om du kan märka ögonblick där du kan visa dig själv, vad du känner, behöver eller tänker och försök att vara lite mer öppen.\n"
        },
        "conversations": [
          {
            "conversationId": "JEQeL6RPUbmDkdEaaJ94bc",
            "title": "Visar dig själv"
          }
        ]
      },
      {
        "id": "2f4980c2-62c2-4685-b7e5-3cd0dea02151",
        "name": "Svarar bra",
        "index": 2,
        "time": "40 min",
        "description": "Lär dig att känna igen och svara när andra vidtar åtgärder för att fördjupa en relation med dig",
        "completionScreen": {
          "content": "Att lära sig att svara bra är en livslång process för de flesta av oss. Det är inte lätt. Börja med att vara en sårbarhetshök. Lägg märke till små ögonblicks sårbarhet hela dagen. Berätta för dig själv - dessa stunder är verkliga och viktiga. Se om du kan svara med säkerhet, validering och vård."
        },
        "conversations": [
          {
            "conversationId": "H38X7X4tuwRdm7rGPLe2dj",
            "title": "Svarar bra"
          }
        ]
      }
    ],
    "rating": 4.4,
    "wip": true,
    "backgroundImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/9a5b146a-61b9-4aef-8519-90304320dcf1/background.jpg"
  },
  "pt": {
    "hideOnHome": false,
    "segments": [
      "ADULTS"
    ],
    "author": {
      "nickname": "Cristina Alves",
      "name": "Cristina Alves"
    },
    "name": "Aprofundar relações",
    "about": {
      "howThisWorks": "#### Como funciona este curso?\n\n* Este curso tem a duração de 4 semanas\n* Uma lição por semana\n* Uma partilha de vídeo por semana",
      "courseValue": "#### O que vou aprender?\n\n* A encontrar coragem para amar\n* A criar confiança para que outros façam o mesmo\n* A aprender a expressar-me plenamente numa relação",
      "body": "Como seria a tua vida se fosses capaz de te expressar plenamente nas relações, com coragem e com amor, de forma a criar segurança para os outros fazerem o mesmo e aceitando e recebendo o amor que as outras pessoas têm para te oferecer?\n\nEste curso explora como desenvolver relações e como dar passos ambiciosos para que o teu verdadeiro eu ganhe vida nessas relações. Se desejas fortalecer uma relação com uma amiga, um amigo, parceira, parceiro, colega ou familiar, este curso pode ajudar-te a começar. A fórmula básica vinda da ciência é: \n\n* **dar tempo x ser corajosa/o x ser amável x estar atenta/o = ligação mais profunda**.",
      "createdBy": {
        "profiles": [
          {
            "name": "Com Cristina Alves"
          }
        ]
      }
    },
    "featured": true,
    "background": {
      "uri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621958192/29k%20FJN/Courses/Menu%20Thumbnails/Cristina_MENU01final_cdxrlk.jpg",
      "color": "#223D37"
    },
    "cta": "Explora e desenvolve competências para criar ligações mais profundas com outros seres humanos.",
    "ratingCount": 460,
    "duration": {
      "count": 4,
      "type": "week"
    },
    "coverUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621852863/29k%20FJN/Courses/Deepening%20Relationships/Photos/DEEPENINGRELATIONSHIPS_COURSECOVER_qiaml9.jpg",
    "tags": [
      "Relationships",
      "Relações"
    ],
    "id": "9a5b146a-61b9-4aef-8519-90304320dcf1",
    "lessons": [
      {
        "id": "bcecd21c-811b-46ac-a0d1-5e549cdfc8b1",
        "name": "Deixar o amor entrar",
        "index": 0,
        "time": "40 min",
        "description": "Aprende a aceitar o amor dos outros e a criar momentos de ligação.",
        "completionScreen": {
          "content": "### Durante a próxima semana:\n\nPara a maioria de nós, aprender a deixar o amor entrar é um processo que dura a vida toda. Esta semana, a tua missão é aperceberes-te das oportunidades para deixar o amor entrar, mesmo as mais pequenas. Observa como te sentes nesses momentos, aceita e abre-te aos sentimentos. Tenta até saboreá-los. Expressa gratidão à pessoa que criou essa oportunidade.\n\nUsa um diário e preenche-o ao fim do dia. Escreve acerca destes tópicos:\n\n* Identificaste alguma oportunidade hoje?\n* O que sentiste no teu corpo?\n* Numa escala de 0 (nada) a 7 (completamente), o quanto conseguiste deixar o amor entrar?\n* Expressaste aceitação à pessoa que criou essa oportunidade?"
        },
        "conversations": [
          {
            "conversationId": "EFSZaU3Tk4dCV5GXPG27bx",
            "title": "Deixar o amor entrar"
          }
        ]
      },
      {
        "id": "9321387f-2b32-4431-aa06-9d663a267b55",
        "name": "Nutrir as relações",
        "index": 1,
        "time": "40 min",
        "description": "Explora as tuas relações e toma medidas para as tornar mais próximas.",
        "completionScreen": {
          "content": "Parabéns por completares **O Mapa das Relações**. Esta semana está atenta/o aos momentos especialmente significativos e nutritivos enquanto eles acontecem. Faz isso abrandando, observando realmente o que tu e a outra pessoa estão a fazer, e tornando-te consciente dos sentimentos e sensações que estás a experienciar no teu corpo."
        },
        "conversations": [
          {
            "conversationId": "S5PVao18y6KQHEBq92u9w4",
            "title": "Nutrir as relações"
          }
        ]
      },
      {
        "id": "05eb4188-e12d-42e5-aaca-ffe258b77780",
        "name": "Revela-te",
        "index": 2,
        "time": "40 min",
        "description": "Cria ligações mais profundas mostrando a tua vulnerabilidade.",
        "completionScreen": {
          "content": "Ser vulnerável pode ser muito assustador. Esta semana, vê se consegues aperceber-te dos momentos em que podes mostrar o que sentes, o que precisas ou o que pensas e tenta ser um pouco mais aberta/o."
        },
        "conversations": [
          {
            "conversationId": "JEQeL6RPUbmDkdEaaJ94bc",
            "title": "Revela-te"
          }
        ]
      },
      {
        "id": "2f4980c2-62c2-4685-b7e5-3cd0dea02151",
        "name": "Responder adequadamente",
        "index": 2,
        "time": "40 min",
        "description": "Aprende a reconhecer e a responder quando os outros dão passos para aprofundar uma relação contigo",
        "completionScreen": {
          "content": "Aprender a responder adequadamente é um processo que dura a vida toda para a maioria de nós. Não é fácil. Começa por ter um olho de lince para a vulnerabilidade. Nota pequenos momentos de vulnerabilidade ao longo do dia. Diz a ti mesma/o - estes momentos são reais e importantes. Vê se podes responder com segurança, validação e cuidado."
        },
        "conversations": [
          {
            "conversationId": "H38X7X4tuwRdm7rGPLe2dj",
            "title": "Responder adequadamente"
          }
        ]
      }
    ],
    "rating": 4.4,
    "wip": false,
    "backgroundImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621852863/29k%20FJN/Courses/Deepening%20Relationships/Photos/DEEPENINGRELATIONSHIPS_COURSECOVER_qiaml9.jpg"
  }
}