import { readdir, readFile, writeFile } from 'fs/promises';

const sectionTypes = {};

const updateButton = (cuid) => (button) => {
  if (
    button.component === 'Button' &&
    button.conversationId &&
    button.conversationId !== cuid
  ) {
    const { sectionId, ...data } = button;

    return {
      ...data,
      component: 'NextConversationButton',
    };
  }

  if (button.component === 'Button' && button.conversationId) {
    const { conversationId, ...data } = button;

    return data;
  }

  if (button.component === 'NextConversationButton' && !button.conversationId) {
    const { conversationId, ...data } = button;

    return {
      ...data,
      component: 'Button',
    };
  }

  if (button.component === 'NextConversationButton' && button.sectionId) {
    const { sectionId, ...data } = button;

    return data;
  }

  if (button.component === 'NextConversationButton' && !button.variant) {
    return {
      ...button,
      variant: 'primary',
    };
  }

  return button;
};

const updateSections = (conversation) => {
  const sections = conversation.sections;

  if (!sections) return conversation;

  return {
    ...conversation,
    sections: sections.map((section) => {
      sectionTypes[section.type] = (sectionTypes[section.type] || 0) + 1;

      switch (section.type) {
        case 'editorialWithImage':
          if (!section.buttons) return section;

          return {
            ...section,
            buttons: section.buttons.map(updateButton(conversation.id)),
          };

        case 'defaultSection':
          return {
            ...section,
            content: section.content.map((sectionContent) => {
              if (sectionContent.component !== 'ButtonGroup')
                return sectionContent;

              return {
                ...sectionContent,
                content: sectionContent.content.map(
                  updateButton(conversation.id),
                ),
              };
            }),
          };

        default:
          return section;
      }
    }),
  };
};

try {
  const basePath = '../src/conversations';
  const files = await readdir(basePath);

  for (const fileName of files) {
    const file = await readFile(`${basePath}/${fileName}`, {
      encoding: 'utf8',
    });
    const conversation = JSON.parse(file);
    const { sections } = conversation;

    const newFile = Object.entries(conversation).reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: updateSections(value),
      }),
      {},
    );

    await writeFile(
      `${basePath}/${fileName}`,
      JSON.stringify(newFile, null, 2),
    );
  }

  console.log(sectionTypes);
} catch (err) {
  console.error(err);
}
