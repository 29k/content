import fs from 'fs';
import path from 'path';
import yargs from 'yargs';
import createRequire from '../../lib/require.cjs';

import { migrateLessons, migrateSteps } from './migrators.js';

const require = createRequire(import.meta.url);

const defaultLanguage = 'en';

const { source } = yargs.option('source', {
  describe: 'source course or exercise',
}).argv;

const sourcePath = path.resolve(source);

if (!fs.existsSync(sourcePath)) {
  console.error(`Can't find ${sourcePath}`);
  process.exit(1);
}

const sourceFile = require(sourcePath);

const output = Object.entries(sourceFile).reduce(
  (languages, [language, data]) => {
    console.log(`Migrating ${language}`);

    const defaultLanguageData = sourceFile[defaultLanguage];
    const { id, name } = defaultLanguageData;

    if ('lessons' in data) {
      console.log(`Migrating course ${name}`);
      return {
        ...languages,
        [language]: {
          ...data,
          lessons: migrateLessons(language, id, data, defaultLanguageData),
        },
      };
    }

    console.log(`Migrating exercise ${name}`);
    /* eslint-disable no-unused-vars */
    const { steps, ...dataWithoutSteps } = data;
    return {
      ...languages,
      [language]: {
        ...dataWithoutSteps,
        steps: undefined,
        conversations: migrateSteps(language, id, name, data),
      },
    };
  },
  {},
);

fs.writeFileSync(sourcePath, JSON.stringify(output, null, 2));

console.log(`Successfully migrated ${source}`);
